package pom_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ContentDeveloper_MetaDataLinking {
     WebDriver driver;
    
    By MetaData_Linking=By.xpath("//a[(@class='btn btn-default') and contains (text () , 'Metadata Linking')]");
    By Add_keyword_link=By.xpath("//*[@id='body']/section[2]/a");
    By Assign_Keyword_Model=By.xpath("//*[@id='MetaDataModel']/form/div[3]/label/span[2]/span[1]/span/ul/li");
    public By KeyWordTypeID = By.id("KeyWordTypeID");
    By DeleteKeyword= By.xpath("//html/body/div[1]/main/div/div[2]/div[2]/section[2]/table/tbody/tr/td[5]/a");


    
    
    public ContentDeveloper_MetaDataLinking(WebDriver driver) {
        this.driver = driver;
	      	}
    
    public void MetaData_Linking(){

		 driver.findElement(MetaData_Linking).click();
	        	             
    }
    
    public void Add_keyword_link(){

		 driver.findElement(Add_keyword_link).click();
	        	             
   }
    
    public void Assign_Keyword_Model(){

		 driver.findElement(Assign_Keyword_Model);
	        	             
  }
    
    public void DeleteKeyword(){

        driver.findElement(DeleteKeyword).click();

    }

}
