package pom_pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
public class SingleContentWriter {
	
	
	WebDriver driver;
	
    public  String OldId="Resources/OldId.txt";
    
    By UnassignedQuestion = By.xpath("//*[@id='body']/div[1]/form/section/div/a");
    
    By SingleContentWriterDev= By.xpath("/html/body/div/main/div/div/section[1]/ul/li[3]/a");

    By RequestGENID= By.xpath("//*[@id='request']");

    By Home_page= By.xpath("/html/body/div/header/div[1]/div/div[1]/a/img");
    
    public  By Approve = By.xpath("//*[@id='Approve']");

    By Submit_To_ContentWriter = By.xpath("//*[@id='Approve']");

    
    
   
    
    public SingleContentWriter(WebDriver driver) {
        this.driver = driver;
	      	}

    
    
    public void Home_page(){

		 driver.findElement(Home_page).click();
	        	             
 }
    
    public void SingleContentWriterDev(){

		 driver.findElement(SingleContentWriterDev).click();
	        	             
}
    
    public void ViewGENID(String view){

		  driver.findElement(By.id(view)).click();
	        	             
  }
    
    public void RequestGENID(){

        driver.findElement(RequestGENID).click();

    }
    
    public void Submit_To_ContentWriter(){

        driver.findElement(Submit_To_ContentWriter).click();

    }
    
    public void Approve(){

        driver.findElement(Approve).click();

    }
    
    public void UnassignedQuestion(){

        driver.findElement(UnassignedQuestion).click();

    }
	


}
