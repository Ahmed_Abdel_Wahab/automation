package pom_pages;
import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

public class LoginPage {
	
	WebDriver driver;
	

    By username = By.name("UserName");

    By password = By.name("password");

    By login = By.xpath("/html/body/div/main/div/div[2]/form/div[3]/input");
    
    public String FilePath="Resources"
    		+ "/GeneratedID.txt";

    public String FilePath2="Resources"
    		+ "/WSName.txt";
    
    public String FilePath3="Resources"
    		+ "/WSIDs.txt";
    
    public static boolean ChromeDriverOption=false;
 
    //Set user name in textbox

    public LoginPage(WebDriver driver) {
        this.driver = driver;
	      	}



	public void setUserName(String strUserName){

        driver.findElement(username).sendKeys("singleWF.automation@nagwa.com");

    }

    

    //Set password in password textbox

    public void setPassword(String strPassword){

         driver.findElement(password).sendKeys("123456");

    }

    

    //Click on login button

    public void clickLogin(){

            driver.findElement(login).click();

    }

    

    /**

     * This POM method will be exposed in test case to login in the application

     * @param strUserName

     * @param strPasword

     * @return

     */

    public void LoginToCDS(String strUserName,String strPasword){

        //Fill user name

        this.setUserName(strUserName);

        //Fill password

        this.setPassword(strPasword);

        //Click Login button

        this.clickLogin();        

  

}
}
