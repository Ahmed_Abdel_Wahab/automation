package pom_pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
public class SingleMathReviewer {
	
	WebDriver driver;
    
    By Single_Math_reviewer= By.partialLinkText("Questions Subject Revie");

    By RequestGENID= By.xpath("//*[@id='request']");
        
    By UnassignedQuestion = By.xpath("/html/body/div/main/div/div[2]/div[1]/form/section/div/a");
    
    By Submit_To_MarkupSpecialist = By.xpath("//*[@id='Approve']");
    
    By Reject_To_ContentDevloper = By.xpath("//*[@id='Reject']");
    
    By Comment = By.id("CommentDiv");

    
    
    public SingleMathReviewer(WebDriver driver) {
        this.driver = driver;
	      	}

    
    public void Submit_To_MathReviewer(){

        driver.findElement(Single_Math_reviewer).click();

    }
    
    public void UnassignedQuestion(){

        driver.findElement(UnassignedQuestion).click();

    }
    
    public void RequestGENID(){

        driver.findElement(RequestGENID).click();

    }
    
    public void ViewGENID(String view){

		  driver.findElement(By.id(view)).click();
	        	             
  }
    
    public void Submit_To_MarkupSpecialist(){

        driver.findElement(Submit_To_MarkupSpecialist).click();

    }
    
    public void Reject_To_ContentDevloper(){

        driver.findElement(Reject_To_ContentDevloper).click();

    }
    
    public void Comment(){

        driver.findElement(Comment).sendKeys("reject test");;

    }
}
