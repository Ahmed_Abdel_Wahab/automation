package pom_pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
public class Single_Markup_reviewer {
	
	
	WebDriver driver;
	
    public  String OldId="Resources/OldId.txt";
    
    public  String GENID_Folder="Resources/";
    
    By SingleMarkupReviewerDev= By.partialLinkText("Questions Markup Revi");

    By RequestGENID= By.xpath("//*[@id='request']");

    By Home_page= By.xpath("/html/body/div/header/div[1]/div/div[1]/a/img");
    
    public By Submit_To_ContentWriter = By.xpath("//*[@id='Approve']");
    
    By Reject_To_MarkupSpecialist = By.xpath("//*[@id='Reject']");


    By Comment = By.id("CommentDiv");
    By Comment2= By.cssSelector("#CommentDiv");

    
    
   
    
    public Single_Markup_reviewer(WebDriver driver) {
        this.driver = driver;
	      	}

    
    
    public void Home_page(){

		 driver.findElement(Home_page).click();
	        	             
 }
    
    public void SingleMarkupReviewerDev(){

		 driver.findElement(SingleMarkupReviewerDev).click();
	        	             
}
    
    public void ViewGENID(String view){

		  driver.findElement(By.id(view)).click();
	        	             
  }
    
    public void RequestGENID(){

        driver.findElement(RequestGENID).click();

    }
    
    public void Submit_To_ContentWriter(){

        driver.findElement(Submit_To_ContentWriter).click();

    }
    
    
    public void Comment(){

        driver.findElement(Comment).sendKeys("reject test");;

    }
    
    public void Comment2(){

        driver.findElement(Comment2).sendKeys("reject test");;

    }
    
    public void Reject_To_MarkupSpecialist(){

        driver.findElement(Reject_To_MarkupSpecialist).click();

    }


}
