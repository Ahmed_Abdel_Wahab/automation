package pom_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EnglishCopyEditorReviewer {

	WebDriver driver;
	 
    By signout= By.xpath(" //*[@id='signout']");
    
    By English_copy_editor_reviewer= By.xpath("/html/body/div/main/div/div/section[5]/ul/li[8]/a");

    By RequestGENID= By.xpath("//*[@id='request']");
    
    By username = By.name("UserName");

    By password = By.name("password");

    By login = By.xpath("/html/body/div/main/div/div[2]/form/div[3]/input");
    
    public By Submit_To_MathReviewer = By.cssSelector("#btnSubmit");

 
    //Set user name in textbox

    public EnglishCopyEditorReviewer(WebDriver driver) {
        this.driver = driver;
	      	}

    
    public void Submit_To_MathReviewer(){

        driver.findElement(Submit_To_MathReviewer).click();

    }
    
    public void ViewGENID(String view){

		  driver.findElement(By.id(view)).click();
	        	             
  }
    
    public void English_copy_editor_reviewer(){

        driver.findElement(English_copy_editor_reviewer).click();

    }
    
    public void RequestGENID(){

        driver.findElement(RequestGENID).click();

    }
    
    
    public void signout(){

        driver.findElement(signout).click();

    }



	public void setUserName(String strUserName){

        driver.findElement(username).sendKeys("dalia.saber@nagwa.com");

    }

    

    //Set password in password textbox

    public void setPassword(String strPassword){

         driver.findElement(password).sendKeys("123456");

    }

    

    //Click on login button

    public void clickLogin(){

            driver.findElement(login).click();

    }

    

    /**

     * This POM method will be exposed in test case to login in the application

     * @param strUserName

     * @param strPasword

     * @return

     */

    public void LoginToCDS(String strUserName,String strPasword){

        //Fill user name

        this.setUserName(strUserName);

        //Fill password

        this.setPassword(strPasword);

        //Click Login button

        this.clickLogin();        

        

    

}


    
}
