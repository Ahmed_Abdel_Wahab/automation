package pom_pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
public class KeywordSpecialist {
	
	
	WebDriver driver;
	
    public  String OldId="Resources/OldId.txt";
    
    By ChooseKeywordMath = By.partialLinkText("Mathematics � Nstes");
        
    By ChooseKeywordChemistry = By.partialLinkText("Nstest1 � Chemist");
    
    By ChooseKeywordWS = By.partialLinkText("Mathemati");

    By KeywordSpecialistDev= By.partialLinkText("KeywordSpecial");

    By ViewID= By.xpath("//*[@id='body']/section[2]/table/tbody/tr[1]/td[5]/a");

    By RequestGENID= By.xpath("//*[@id='request']");

    By Home_page= By.xpath("/html/body/div/header/div[1]/div/div[1]/a/img");
    
    By Submit_To_ContentWriter = By.xpath("//*[@id='Approve']");

    
    
   
    
    public KeywordSpecialist(WebDriver driver) {
        this.driver = driver;
	      	}

    
    
    public void Home_page(){

		 driver.findElement(Home_page).click();
	        	             
 }
    
    public void KeywordSpecialistDev(){

		 driver.findElement(KeywordSpecialistDev).click();
	        	             
}
    
    public void ViewID(){

        driver.findElement(ViewID).click();

    }
    
    public void RequestGENID(){

        driver.findElement(RequestGENID).click();

    }
    
    public void Submit_To_ContentWriter(){

        driver.findElement(Submit_To_ContentWriter).click();

    }
    
    public void Approve(String appro){

        driver.findElement(By.id(appro)).click();

    }
    
    public void edit(String edi){

        driver.findElement(By.id(edi)).click();

    }
    
    
    
    public void ChooseKeywordMath(){

        driver.findElement(ChooseKeywordMath).click();

    }
    
    public void ChooseKeywordChemistry(){

        driver.findElement(ChooseKeywordChemistry).click();

    }
	
    public void ChooseKeywordWS(){

        driver.findElement(ChooseKeywordWS).click();

    }


}
