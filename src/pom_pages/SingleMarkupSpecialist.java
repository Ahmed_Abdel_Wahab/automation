package pom_pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
public class SingleMarkupSpecialist {
	
	
    public By ViewID2= By.xpath("//*[@id='list']/section[2]/table/tbody/tr[1]/td[6]/a");

	WebDriver driver;
	
    public  String OldIdSingle="Resources/single/OldId.txt";
    
    public  String OldIdChemistry="Resources/Chemistry/OldId.txt";

    
    public  String GENID_Folder_Single=System.getProperty("user.dir") + "\\" + "\\Resources\\Single\\";
    
    public  String GENID_Folder_Chemistry=System.getProperty("user.dir") + "\\" + "\\Resources\\Chemistry\\";

    
    By SingleMarkupSpecialistDev= By.partialLinkText("Questions Markup Special");

    By RequestGENID= By.id("request");

    By Status= By.xpath("//*[@id='list']/section[2]/table/tbody/tr[1]/td[5]");

    By Home_page= By.xpath("/html/body/div/header/div[1]/div/div[1]/a/img");
    
    By Submit_To_MarkupReviewer = By.cssSelector("#Approve");

    
    
   
    
    public SingleMarkupSpecialist(WebDriver driver) {
        this.driver = driver;
	      	}

    
    
    public void Home_page(){

		 driver.findElement(Home_page).click();
	        	             
 }
    
    public void SingleMarkupSpecialistDev(){

		 driver.findElement(SingleMarkupSpecialistDev).click();
	        	             
}
    
    public void ViewGENID(String view){

		  driver.findElement(By.id(view)).click();
	        	             
  }
    public void ViewID2(){

        driver.findElement(ViewID2);

    }
    
    public void RequestGENID(){

        driver.findElement(RequestGENID).click();

    }
    
    public String Status(){

        return driver.findElement(Status).getText();

    }
    
    public void Submit_To_MarkupReviewer(){

        driver.findElement(Submit_To_MarkupReviewer).click();

    }


}
