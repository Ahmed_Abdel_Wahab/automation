package pom_pages;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

public class ContentDeveloper {
	
    WebDriver driver;
    
    By ContentDeveloper = By.partialLinkText("Questions Content Develop");  
    
    By UnassignedQuestion = By.xpath("/html/body/div/main/div/div/a[2]");
    
    By Worksheet = By.xpath("/html/body/div/main/div/div[1]/a[1]");
    
    By Worksheet_Name = By.id("worksheetName");
    
    By numberOfQuestions = By.id("numberOfQuestions");
    
    By WorkSheet_Save = By.id("saveBtn");


    public String subject = "Mathematics";
    
    public By SubjectType = By.xpath("//*[@id='subjectType']");
    
    public By WorkSheetName = By.xpath("//*[@id='worksheetID']");
    
    public By WFSubjectID = By.xpath("//*[@id='subjectId']");

    
    By Keyword_DropDownList=By.xpath("//*[@id='body']/form/div/input[1]");
    
    By ViewGENID=By.xpath("//*[@id='body']/section[2]/table/tbody/tr[1]/td[3]/a");
    
    By GENID=By.xpath("/html/body/div/main/div/div[2]/section[2]/table/tbody/tr[1]/td[1]");

        
    public By MetaDataModel = By.xpath("//*[@id='MetaDataModel']/form/div[3]/label/span[2]/span[1]/span/ul/li/input");

    By AddKeyword=By.id("AddKeyword");
    
    By Note= By.cssSelector(".btn-link-blue");
        
    By  Distractor_creator=By.xpath("//*[@id='btnSubmit']");
    
    By  WS_Distractor=By.id("btnDistractorCreator");
    
    By  WS_CopyEditor=By.id("btnCopyEditor");

    By  WS_MarkupSpecialist=By.id("btnMarkupSpecialist");

    By  Save_Draft=By.xpath("//*[@id='btnSaveDraft']");
    
    By  Error_Msg= By.xpath("//*[@id='Error']");
    
    By  validation= By.xpath("//*[@id='validation']");
    
    By  Subject_validation= By.xpath("//*[@id='validation']");
    
    public By  WorkSheetQ1= By.xpath("//*[@id='singleId0']");

    public By  WorkSheetQ2= By.xpath("//*[@id='singleId1']");    
    
    public By  WorkSheetQ3= By.xpath("//*[@id='singleId2']");    


    By  close_developer_note= By.cssSelector("a.btn-link:nth-child(2)");
    
    public String validation_msg1="Please Compelete Question Data";
    
    public String validation_msg2="You have to insert root keyword";

    public String validation_msg3="Sucess 'transfered to distractor creator'";
    
    public String validation_error="Please Select Subject";

    
    By Home_page= By.xpath("/html/body/div/header/div[1]/div/div[1]/a/img");





    public void Home_page(){

		 driver.findElement(Home_page).click();
	        	             
  }
        
    
    public ContentDeveloper(WebDriver driver) {
        this.driver = driver;
	      	}



	public void clickContentDeveloper(){

        driver.findElement(ContentDeveloper).click();

    }
	
	public void UnassignedQuestion(){

        driver.findElement(UnassignedQuestion).click();

    }
	
	public void Subject_DropDownList(){

        driver.findElement(Keyword_DropDownList).click();

    }
	
	public  void ViewGENID(String actual_id2){
	  
		By GENID=By.id(actual_id2);
        driver.findElement(GENID).click();

    }
	
	public  String GENID(){

        return driver.findElement(GENID).getText();

    }
	public void AddKeyword(){

        driver.findElement(AddKeyword).click();

    }
	
	
	
	public void Note(){

        driver.findElement(Note).click();

    }
	
	public void Save_Draft(){

        driver.findElement(Save_Draft).click();

    }
	
	public void Distractor_creator(){

        driver.findElement(Distractor_creator).click();

    }
	
	public String Error_Msg(){

        return driver.findElement(Error_Msg).getText();

    }
	
	public String validation(){

        return driver.findElement(validation).getText();

    }
	
	public String Subject_validation(){

        return driver.findElement(Subject_validation).getText();

    }
	
	public void close_developer_note(){

        driver.findElement(close_developer_note).click();

    }
	
	public void Worksheet(){

        driver.findElement(Worksheet).click();

    }
	
	public String Worksheet_Name(String uniqueID){

		
        driver.findElement(Worksheet_Name).sendKeys("WorkSheet Name",uniqueID);
		return null;

    }
	
	
	public void WorkSheet_Save(){

        driver.findElement(WorkSheet_Save).click();

    }
	
	public void WS_Distractor(){

        driver.findElement(WS_Distractor).click();

    }
	
}

