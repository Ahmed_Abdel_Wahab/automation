package pom_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DistractorEditor {
	
	
	WebDriver driver;
	    
    By SingleDistractor= By.partialLinkText("Questions Distractor Edi");
    
    By unassigned_question= By.xpath("/html/body/div/main/div/div[2]/div[1]/form/section/div/a");

    By RequestGENID= By.xpath("/html/body/div/main/div/div[2]/form/div/input[1]");
    
    By Submit_to_copyEditor=By.xpath("//*[@id='btnSubmit']");

    By AddDistractor=By.xpath("/html/body/div[2]/main/div/div/form/div/div/div[2]/question-template/div[3]/div/mcq-template/div[2]/ul/li/a");
    
    

    
    
    public DistractorEditor(WebDriver driver) {
        this.driver = driver;
	      	}

    
    
    public void SingleDistractor(){

 		 driver.findElement(SingleDistractor).click();
 	        	             
    }
    
    public void unassigned_question(){

 		 driver.findElement(unassigned_question).click();
 	        	             
    }
    
    public void RequestGENID(){

		 driver.findElement(RequestGENID).click();
	        	             
   }
    
    public void ViewGENID(String view){

		  driver.findElement(By.id(view)).click();
	        	             
  }
    
    public void AddDistractor(){

		 driver.findElement(AddDistractor).click();
	        	             
 }
    
    public void Submit_to_copyEditor(){

		 driver.findElement(Submit_to_copyEditor).click();
	        	             
 }


}
