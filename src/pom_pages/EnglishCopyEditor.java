package pom_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EnglishCopyEditor {

	WebDriver driver;
	 
    By Home_page= By.xpath("/html/body/div/header/div[1]/div/div[1]/a/img");
    
    By EnglishCopyEditor= By.xpath("/html/body/div/main/div/div/section[1]/ul/li[7]/a");

    By RequestGENID= By.xpath("/html/body/div/main/div/div[2]/form/div/input[1]");
    
    By EnglishCopyEditorReviewer = By.cssSelector("#btnSubmit");


    public EnglishCopyEditor(WebDriver driver) {
        this.driver = driver;
	      	}

    
    public void Home_page(){

		 driver.findElement(Home_page).click();
	        	             
  }
    
	public void EnglishCopyEditorDev() {
		
		 driver.findElement(EnglishCopyEditor).click();
		
	}
    
    public void RequestGENID(){

		 driver.findElement(RequestGENID).click();
	        	             
  }
    
    public void ViewGENID(String view){

 
		 driver.findElement(By.id(view)).click();

	        	             
  }
    
    public void EnglishCopyEditorReviewer(){

		 driver.findElement(EnglishCopyEditorReviewer).click();
	        	             
}


    
}
