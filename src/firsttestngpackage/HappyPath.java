package firsttestngpackage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.apache.commons.io.FileUtils;
import pom_pages.ContentDeveloper;
import pom_pages.ContentDeveloper_MetaDataLinking;
import pom_pages.ContentDeveloper_Question;
import pom_pages.DistractorEditor;
import pom_pages.EnglishCopyEditor;
import pom_pages.EnglishCopyEditorReviewer;
import pom_pages.KeywordSpecialist;
import pom_pages.LoginPage;
import pom_pages.SingleContentWriter;
import pom_pages.SingleMarkupSpecialist;
import pom_pages.SingleMathReviewer;
import pom_pages.Single_Markup_reviewer;

public class HappyPath {

	WebDriver driver;
	LoginPage objLogin;
	ContentDeveloper objContentDeveloper;
	ContentDeveloper_Question objContentDeveloper_Question;
	ContentDeveloper_MetaDataLinking objContentDeveloper_MetaDataLinking;
	DistractorEditor objDistractorEditor;
	EnglishCopyEditor objEnglishCopyEditor;
	EnglishCopyEditorReviewer objEnglishCopyEditorReviewer;
	SingleMathReviewer objSingleMathReviewer;
	SingleMarkupSpecialist objSingleMarkupSpecialist;
	Single_Markup_reviewer objSingleMarkupreviewer;
	SingleContentWriter  objSingleContentWriter;
	KeywordSpecialist  objKeywordSpecialist;



	@BeforeTest

	public void setup() throws InterruptedException{

		System.setProperty("webdriver.chrome.driver","Resources/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.setHeadless(LoginPage.ChromeDriverOption);
		driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.get("http://testing.cds.nagwa.com");
		driver.navigate().refresh();
		Thread.sleep(400);

	}


	@Test
	//SingleContentDeveloper

	public void SingleWorkflowHappyPath() throws InterruptedException, IOException {

		//Declaration
		WebDriverWait wait = new WebDriverWait(driver,25000);
		objLogin = new LoginPage(driver);
		objLogin.LoginToCDS("", "");
		System.out.println("Single Workflow Happy Path");
		objContentDeveloper = new ContentDeveloper(driver);
		objContentDeveloper_Question = new ContentDeveloper_Question(driver);
		objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
		objDistractorEditor = new DistractorEditor(driver);
		objEnglishCopyEditor = new EnglishCopyEditor(driver);
		objEnglishCopyEditorReviewer = new EnglishCopyEditorReviewer(driver);
		objSingleMathReviewer = new SingleMathReviewer(driver);
		objSingleMarkupSpecialist = new SingleMarkupSpecialist(driver);
		objSingleMarkupreviewer = new Single_Markup_reviewer(driver);
		objSingleContentWriter = new SingleContentWriter(driver);
		objKeywordSpecialist = new KeywordSpecialist(driver);	

		//Content Developer
		objContentDeveloper.clickContentDeveloper();
		objContentDeveloper.UnassignedQuestion();
		WebElement subject=driver.findElement(objContentDeveloper.SubjectType);
		Select ab= new Select(subject);
		ab.selectByVisibleText(objContentDeveloper.subject);
		objContentDeveloper.Subject_DropDownList();
		String actual_id2="view"+objContentDeveloper.GENID();      	  	      
		System.out.println(actual_id2);
		objContentDeveloper.ViewGENID(actual_id2);
		Thread.sleep(1000);
		objContentDeveloper_Question = new ContentDeveloper_Question(driver);
		String actual_id=objContentDeveloper_Question.SingleID();      	  	      
		String titleId="title"+actual_id;
		String ViewId="view"+actual_id;
		String Dialect="Dialect"+actual_id;
		String UnitSystem="UnitSystem"+actual_id;
		String Currency="Currency"+actual_id;
		String Calendar="Calendar"+actual_id;
		
		Select dropdown=new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(Dialect))));
		dropdown.selectByVisibleText("american");
		Thread.sleep(300);
		dropdown=new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(UnitSystem))));
		dropdown.selectByVisibleText("si");
		Thread.sleep(300);
		dropdown=new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(Currency))));
		dropdown.selectByVisibleText("usd");
		Thread.sleep(300);
		dropdown=new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(Calendar))));
		dropdown.selectByVisibleText("gregorian");
		Thread.sleep(300);

		driver.findElement(By.xpath("//span[(@class='label') and contains (text () , 'False')]")).click();  	        
		driver.findElement(By.id(titleId)).sendKeys("Test Title");  	        
		String QuestionId="question"+actual_id;
		String KeyId="key"+actual_id;
		Thread.sleep(2000);
		String QuestionPath= "//div[contains(@id,'"+QuestionId+"')]";
		String KeyPath= "//div[contains(@id,'"+KeyId+"')]";
		driver.findElement(By.xpath(QuestionPath)).sendKeys("Test question data");
		Thread.sleep(300);
		driver.findElement(By.xpath(KeyPath)).sendKeys("Test key value data");
		Thread.sleep(300);
		objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
		objContentDeveloper_MetaDataLinking.MetaData_Linking();
		Thread.sleep(500);
		String oldTab = driver.getWindowHandle();
		ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		newTab.remove(oldTab);
		driver.switchTo().window(newTab.get(0));
		objContentDeveloper_MetaDataLinking.Add_keyword_link();
		Thread.sleep(500);
		for ( String currentwindow : driver.getWindowHandles())driver.switchTo( ).window(currentwindow);
		{
			Thread.sleep(400);
			dropdown=new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(objContentDeveloper_MetaDataLinking.KeyWordTypeID)));
			dropdown.selectByIndex(1);
			Thread.sleep(300);
			objContentDeveloper_MetaDataLinking.Assign_Keyword_Model();
			Thread.sleep(100);
			WebElement objKw=driver.findElement(objContentDeveloper.MetaDataModel);
			objKw.sendKeys("Nstest");
			objKw.sendKeys(Keys.ENTER);  
			Thread.sleep(300);
			objContentDeveloper.AddKeyword();
			Thread.sleep(300);
			driver.close();		                    
		}   		           
		driver.switchTo().window(oldTab);
		Thread.sleep(300);           
		String Note="Note"+actual_id;
		objContentDeveloper.Note();
		driver.findElement(By.id(Note)).sendKeys("Test developer note data");        
		String addComment="addComment"+actual_id;
		driver.findElement(By.id(addComment)).click();
		Thread.sleep(200); 
		objContentDeveloper.close_developer_note();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='btnSubmit']")));
		objContentDeveloper.Distractor_creator();
		Thread.sleep(500);		    
		try {
			Thread.sleep(300);
		}catch (InterruptedException e)	 {
			String error_msg=objContentDeveloper.Error_Msg();
			String error_msg2=objContentDeveloper.validation();
			if (error_msg.equals(objContentDeveloper.validation_msg1))
			{
				System.out.println("Fail 'incomplete data'");
			}
			else if (error_msg2.equals(objContentDeveloper.validation_msg2))
			{
				System.out.println("Fail 'No Root Keyword'");
			}
			else
			{
				System.out.println(objContentDeveloper.validation_msg3);
			}

		}
		final String newLine1 = System.getProperty("line.separator");
		try {
			FileWriter writer = new FileWriter(objLogin.FilePath, true);

			writer.write(newLine1 + actual_id);
			writer.write('\n');   
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Thread.sleep(500);


		//Second Role ' Distractor creator' ,assume same user have both roles'Navigate to Home page'		        
		objContentDeveloper.Home_page();
		Thread.sleep(200);
		objDistractorEditor.SingleDistractor();
		Thread.sleep(300);
		objDistractorEditor.unassigned_question();
		WebElement subject2=driver.findElement(objContentDeveloper.SubjectType);
		Select ab2= new Select(subject2);
		ab2.selectByVisibleText(objContentDeveloper.subject);
		Thread.sleep(200);	      
		for(int x=0;x<4;x++)
		{
			objDistractorEditor.RequestGENID();

		}

		Thread.sleep(300);
		driver.navigate().refresh();
		objDistractorEditor.ViewGENID(ViewId); 
		Thread.sleep(600);
		objDistractorEditor.Submit_to_copyEditor();
		objEnglishCopyEditor.Home_page();


		//Third Role ' English Copy Editor ' ,assume same user have both roles'Navigate to Home page'		        
		objEnglishCopyEditor.EnglishCopyEditorDev();
		WebElement subject3=driver.findElement(objContentDeveloper.SubjectType);
		Select ab3= new Select(subject3);
		ab3.selectByVisibleText(objContentDeveloper.subject);		     	        
		for(int x=0;x<4;x++)
		{
			objEnglishCopyEditor.RequestGENID();

		}

		Thread.sleep(300);
		driver.navigate().refresh();
		Thread.sleep(1000);
		objEnglishCopyEditor.ViewGENID(ViewId);
		Thread.sleep(1000);
		driver.navigate().refresh();
		Thread.sleep(1000);
		objEnglishCopyEditor.EnglishCopyEditorReviewer();
		Thread.sleep(500);


		//Fourth Role ' English copy editor reviewer' ,assume same user have both roles'Navigate to Home page'
		//need to log out then login in different user
		objEnglishCopyEditorReviewer.signout();
		Thread.sleep(500);
		objEnglishCopyEditorReviewer.LoginToCDS("", "");
		Thread.sleep(500);	
		objEnglishCopyEditorReviewer.English_copy_editor_reviewer();
		//Select subject by visible text and request ID
		WebElement subject4=driver.findElement(objContentDeveloper.SubjectType);
		Select ab4= new Select(subject4);
		ab4.selectByVisibleText(objContentDeveloper.subject);
		for(int x=0;x<4;x++)
		{
			objEnglishCopyEditorReviewer.RequestGENID();

		}

		Thread.sleep(1000);
		driver.navigate().refresh();
		objEnglishCopyEditorReviewer.ViewGENID(ViewId);
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(objEnglishCopyEditorReviewer.Submit_To_MathReviewer));
		objEnglishCopyEditorReviewer.Submit_To_MathReviewer();
		Thread.sleep(500);


		//Fifth Role ' Math reviewer' ,assume same user have both roles'Navigate to Home page'
		objEnglishCopyEditorReviewer.signout();
		Thread.sleep(500);
		objLogin.LoginToCDS("", "");
		Thread.sleep(200);	
		objSingleMathReviewer.Submit_To_MathReviewer();
		Thread.sleep(200);
		objSingleMathReviewer.UnassignedQuestion();
		WebElement subject5=driver.findElement(objContentDeveloper.SubjectType);
		Select ab5= new Select(subject5);
		ab5.selectByVisibleText(objContentDeveloper.subject);
		for(int x=0;x<4;x++)
		{
			objSingleMathReviewer.RequestGENID();

		}
		Thread.sleep(300);
		driver.navigate().refresh();	
		objSingleMathReviewer.ViewGENID(ViewId);
		Thread.sleep(600);
		objSingleMathReviewer.Submit_To_MarkupSpecialist();	      	      




		//Fifth role 'Single Markup specialist 	      
		//Now we get ID of the single id 'old one' from txt file

		
		String oldID=null;
		//overwrite oldid with newid in txt file at both mathematics and chemistry depend on subject
		Thread.sleep(500);
		if(objContentDeveloper.subject=="Mathematics")
		{
			final FileReader namereader = new FileReader(objSingleMarkupSpecialist.OldIdSingle);
			final BufferedReader in = new BufferedReader(namereader);
			 oldID=in.readLine();    
			//execute rename oldid* actualID'New id' *
			try {
				String new_dir=objSingleMarkupSpecialist.GENID_Folder_Single;
				Runtime rt = Runtime.getRuntime();		            

				String newID=actual_id;
				String cmd = "cmd.exe /c ren" + " " +oldID+"*"+" "+newID+"*" ;
				rt.exec(cmd.toString(), null, new File(new_dir));
			} catch (IOException e)
			{
				System.out.println("FROM CATCH" + e.toString());
			}
			FileWriter writer = new FileWriter(objSingleMarkupSpecialist.OldIdSingle, false);
			writer.write(actual_id);
			writer.close();
			in.close();

		}
		else
		{
			final FileReader namereader = new FileReader(objSingleMarkupSpecialist.OldIdChemistry);
			final BufferedReader in = new BufferedReader(namereader);
		     oldID=in.readLine();    
			//execute rename oldid* actualID'New id' *
			try {
				String new_dir=objSingleMarkupSpecialist.GENID_Folder_Chemistry;
				Runtime rt = Runtime.getRuntime();		            

				String newID=actual_id;
				String cmd = "cmd.exe /c ren" + " " +oldID+"*"+" "+newID+"*" ;
				rt.exec(cmd.toString(), null, new File(new_dir));
			} catch (IOException e)
			{
				System.out.println("FROM CATCH" + e.toString());
			}
			FileWriter writer = new FileWriter(objSingleMarkupSpecialist.OldIdChemistry, false);
			writer.write(actual_id);
			writer.close();
			in.close();

		}

		System.out.println(oldID);
		System.out.println(actual_id);	       
		Thread.sleep(1000);




		objSingleMarkupSpecialist.Home_page();
		Thread.sleep(200);
		objSingleMarkupSpecialist.SingleMarkupSpecialistDev();
		driver.navigate().refresh();
		Thread.sleep(500);
		WebElement subject6=driver.findElement(objContentDeveloper.SubjectType);
		for(int x=0;x<4;x++)
		{
			Select ab61= new Select(subject6);
			ab61.selectByVisibleText(objContentDeveloper.subject);
			Thread.sleep(300);
			objSingleMarkupSpecialist.RequestGENID();

		}
		Thread.sleep(3000);
		driver.navigate().refresh();
		Thread.sleep(500);
		driver.navigate().refresh();

		//Ordinary mathematics single question
		if(objContentDeveloper.subject=="Mathematics")
		{
			String xml=objSingleMarkupSpecialist.GENID_Folder_Single+actual_id+".single.xml";
			File inputXML = new File(xml);
			FileReader fr = new FileReader(inputXML);
			String s1= "";   
			StringBuilder strTotale = new StringBuilder();
			BufferedReader br = new BufferedReader(fr);
			try {
				while ((s1 = br.readLine()) != null) {
					s1=s1.replaceAll(oldID, actual_id);
					strTotale.append(s1);
				}
			} catch ( IOException  e) {
				e.printStackTrace();
			}  
			finally
			{
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			PrintWriter out = new PrintWriter(xml);       
			out.println(strTotale);
			out.close();
			//Xml content
			System.out.println(strTotale.toString()); 	

			WebElement element = driver.findElement(By.id("file"));
			element.sendKeys(xml);	

		}
		//Chemistry single question

		else
		{
			String xml=objSingleMarkupSpecialist.GENID_Folder_Chemistry+actual_id+".single.xml";
			String cdxml=objSingleMarkupSpecialist.GENID_Folder_Chemistry+actual_id+".1.cdxml";
			String svg=objSingleMarkupSpecialist.GENID_Folder_Chemistry+actual_id+".1.svg";

			//Xml
			String allFiles=xml+" \n "+cdxml+" \n "+svg;
			File inputXML = new File(xml);
			FileReader fr = new FileReader(inputXML);
			String s1= "";   
			StringBuilder strTotale = new StringBuilder();
			BufferedReader br = new BufferedReader(fr);
			try {
				while ((s1 = br.readLine()) != null) {
					s1=s1.replaceAll(oldID, actual_id);
					strTotale.append(s1);
				}
			} catch ( IOException  e) {
				e.printStackTrace();
			}  
			finally
			{
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			PrintWriter out = new PrintWriter(xml);       
			out.println(strTotale);
			out.close();
			//Xml content
			System.out.println(strTotale.toString()); 
			//CDXML
			File inputCDXML = new File(cdxml);
			FileReader fr1 = new FileReader(inputCDXML);
			String s11= "";   
			StringBuilder strTotale1 = new StringBuilder();
			BufferedReader br1 = new BufferedReader(fr1);
			try {
				while ((s11 = br1.readLine()) != null) {
					s11=s11.replaceAll(oldID, actual_id);
					strTotale1.append(s11);
				}
			} catch ( IOException  e) {
				e.printStackTrace();
			}  
			finally
			{
				try {
					br1.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			PrintWriter out1 = new PrintWriter(cdxml);       
			out1.println(strTotale1);
			out1.close();
			//CDXML content
			System.out.println(strTotale1.toString()); 
			

			WebElement element = driver.findElement(By.id("file"));
			element.sendKeys(allFiles);	
		}

		String Status=null;
		Thread.sleep(8000); 
		Status=objSingleMarkupSpecialist.Status();
		System.out.println(Status.toString());
		driver.navigate().refresh();
		Thread.sleep(500);
		driver.navigate().refresh();
		while (Status.contains("Processing"))
		{
			Thread.sleep(6000);
			driver.navigate().refresh();
			Thread.sleep(2000);
			Status=objSingleMarkupSpecialist.Status();
			Thread.sleep(2000);
			//Check ID status
			//System.out.println(Status.toString());
		}

		System.out.println(Status.toString()); 
		objSingleMarkupSpecialist.ViewGENID(ViewId);
		Thread.sleep(2000);
		driver.navigate().refresh();
		Thread.sleep(2000);
		objSingleMarkupSpecialist.Submit_To_MarkupReviewer();
		Thread.sleep(4000);	      

		//Role six : Markup reviewer
		objSingleMarkupreviewer.Home_page();
		Thread.sleep(200);
		objSingleMarkupreviewer.SingleMarkupReviewerDev();
		Thread.sleep(200);
		WebElement subject7=driver.findElement(objContentDeveloper.SubjectType);
		Select ab7= new Select(subject7);
		ab7.selectByVisibleText(objContentDeveloper.subject);
		for(int x=0;x<4;x++)
		{
			objSingleMarkupreviewer.RequestGENID();

		}

		Thread.sleep(5000);
		objSingleMarkupreviewer.ViewGENID(ViewId);
		Thread.sleep(3000);
		objSingleMarkupreviewer.Submit_To_ContentWriter();
		Thread.sleep(3000);
		objSingleContentWriter.Home_page();
		Thread.sleep(200);


		//Role seven :Content Writer
		objSingleContentWriter.SingleContentWriterDev();
		objSingleContentWriter.UnassignedQuestion();
		WebElement subject8=driver.findElement(objContentDeveloper.SubjectType);
		Select ab8= new Select(subject8);
		ab8.selectByVisibleText(objContentDeveloper.subject);
		for(int x=0;x<4;x++)
		{
			objSingleContentWriter.RequestGENID();

		}

		Thread.sleep(300);
		driver.navigate().refresh();
		objSingleContentWriter.ViewGENID(ViewId);
		Thread.sleep(500);
		driver.navigate().refresh();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(objSingleContentWriter.Approve));
		objSingleContentWriter.Approve();
		Thread.sleep(1000);
		objSingleContentWriter.Home_page();
		Thread.sleep(200);
		objKeywordSpecialist.KeywordSpecialistDev();
		Thread.sleep(4000);

		//Role eight :Keyword Specialist
		if (objContentDeveloper.subject=="Mathematics")
		{
		
			objKeywordSpecialist.ChooseKeywordMath();
		}
		else
		{
			objKeywordSpecialist.ChooseKeywordChemistry();
			
		}
		Thread.sleep(600);
		String edit="edit"+actual_id;
		String approve="approve"+actual_id;
		//Creating our own Path to access elements as IDs of elements aren't fixed and unique per ID and can't be included into POM
		//*[@id="single486169485643"]/div[3]/a
		Thread.sleep(3000);
		driver.navigate().refresh();
		Thread.sleep(9000);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath1))).click();
		objKeywordSpecialist.edit(edit);
		Thread.sleep(5000);
		driver.navigate().refresh();
		Thread.sleep(20000);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath2))).click();
		//driver.findElement(By.id(approve)).click();
		objKeywordSpecialist.Approve(approve);
		/////////Print
		System.out.println("Aprroved ID is "+actual_id); 
		System.out.println("Aprroved ID Keyword are Nstest1 . Mathematics"); 


	}
	@AfterMethod 
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException { 
		if (testResult.getStatus() == ITestResult.FAILURE) { 
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); 
			FileUtils.copyFile(scrFile, new File("errorScreenshots\\" + testResult.getName() + "-" 
					+ Arrays.toString(testResult.getParameters()) +  ".jpg"));
		} 
	}


	@AfterTest

	public void End() throws InterruptedException{

		driver.close();
	}


}
