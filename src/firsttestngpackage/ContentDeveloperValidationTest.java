package firsttestngpackage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pom_pages.ContentDeveloper;
import pom_pages.ContentDeveloper_MetaDataLinking;
import pom_pages.ContentDeveloper_Question;

import pom_pages.LoginPage;

public class ContentDeveloperValidationTest {
	
	    WebDriver driver;
	    LoginPage objLogin;
	    ContentDeveloper objContentDeveloper;
	    ContentDeveloper_Question objContentDeveloper_Question;
	    ContentDeveloper_MetaDataLinking objContentDeveloper_MetaDataLinking;

	    
	    @BeforeTest

	    public void setup() throws InterruptedException{

		    System.setProperty("webdriver.chrome.driver","Resources/chromedriver.exe");
		    ChromeOptions options = new ChromeOptions();
		    options.setHeadless(LoginPage.ChromeDriverOption);
	        driver = new ChromeDriver(options);
	        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		    driver.get("http://testing.cds.nagwa.com");
		    driver.navigate().refresh();
		    Thread.sleep(400);
			objLogin = new LoginPage(driver);
		    objLogin.LoginToCDS("", "");
			objContentDeveloper = new ContentDeveloper(driver);
			objContentDeveloper_Question = new ContentDeveloper_Question(driver);
			objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
	    }
	    
	    
	    @Test(priority=1)
		  //SingleContentDeveloper
		  public void Check_Request_Without_Subject_Validation() throws InterruptedException, IOException {			  
			  //Declaration
			  System.out.println("Single Content Developer Validation Method1 TestCase1 'Check required subject selection before request validation");
		      //Content Developer
		      objContentDeveloper.clickContentDeveloper();
		      objContentDeveloper.UnassignedQuestion();
		      objContentDeveloper.Subject_DropDownList();
		      Thread.sleep(500);
		      String Msg=objContentDeveloper.Subject_validation();
		      if (Msg.equals(objContentDeveloper.validation_error))
			     {
			     System.out.println("***Passed1!***:Single Content Developer Validation Method1 TestCase1 'Check required subject selection before request validation");
			     }
			  else
			     {
			     System.out.println("Failed!");
			     }
		      

	    }
	    
	    @Test(priority=2)
		  //SingleContentDeveloper
		  public void Check_Key_Value_Validation() throws InterruptedException, IOException {			  
	    	 //Declaration
			  System.out.println("Single Content Developer Validation Method2 TestCase2 'Check required Question Data validation");
		      WebDriverWait wait = new WebDriverWait(driver,25000);

		      //Back to home page
		      objContentDeveloper.Home_page();
		      Thread.sleep(600);
		      //Content Developer
		      objContentDeveloper.clickContentDeveloper();
		      objContentDeveloper.UnassignedQuestion();
		      WebElement subject=driver.findElement(objContentDeveloper.SubjectType);
		      Select ab= new Select(subject);
		      ab.selectByVisibleText("Mathematics");
		      objContentDeveloper.Subject_DropDownList();
		      String actual_id2="view"+objContentDeveloper.GENID();      	  	      
		      System.out.println(actual_id2);
		      objContentDeveloper.ViewGENID(actual_id2);
		      Thread.sleep(1000);
		      objContentDeveloper_Question = new ContentDeveloper_Question(driver);
		      String actual_id=objContentDeveloper_Question.SingleID();      	  	      
		      String titleId="title"+actual_id;
		      driver.findElement(By.id(titleId)).sendKeys("Test Title");  	        
		      String QuestionId="question"+actual_id; //Check required question data validation
		      //String KeyId="key"+actual_id;  
		      Thread.sleep(500);
		      driver.findElement(By.id(QuestionId)).sendKeys("Test question data"); //Check required question data validation
		      //driver.findElement(By.id(KeyId)).sendKeys("Test key value data");   
		      objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
		      objContentDeveloper_MetaDataLinking.MetaData_Linking();
		      Thread.sleep(500);
		      String oldTab = driver.getWindowHandle();
		      ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		      newTab.remove(oldTab);
		      driver.switchTo().window(newTab.get(0));
		      objContentDeveloper_MetaDataLinking.Add_keyword_link();
		      Thread.sleep(500);
		      for ( String currentwindow : driver.getWindowHandles())driver.switchTo( ).window(currentwindow);
		                
		      {
			               Thread.sleep(400);
		                   Select dropdown=new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(objContentDeveloper_MetaDataLinking.KeyWordTypeID)));
		                   dropdown.selectByIndex(1);
		                   Thread.sleep(300);
		                   objContentDeveloper_MetaDataLinking.Assign_Keyword_Model();
		                   Thread.sleep(100);
		                   WebElement objKw=driver.findElement(objContentDeveloper.MetaDataModel);
		                   objKw.sendKeys("Nstest");
		                   objKw.sendKeys(Keys.ENTER);  
		                   Thread.sleep(300);
	                       objContentDeveloper.AddKeyword();
		                   Thread.sleep(300);
		                   driver.close();		                    
		       }  
		      
		      driver.switchTo().window(oldTab);
		      Thread.sleep(300);    
		      
		      //Optional note entry
		      String Note="Note"+actual_id;
	          objContentDeveloper.Note();
		 	  driver.findElement(By.id(Note)).sendKeys("Test developer note data");        
		 	  String addComment="addComment"+actual_id;
		      driver.findElement(By.id(addComment)).click();
		      Thread.sleep(200); 
		      objContentDeveloper.close_developer_note();
		 	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='btnSubmit']")));
		 	  objContentDeveloper.Save_Draft();//Save draft before check validation 
		      Thread.sleep(500);
		      driver.navigate().refresh();
		      Thread.sleep(1000);
		      
              //String s=driver.getCurrentUrl();
			  //System.out.println(s);

		      
		      objContentDeveloper.Distractor_creator();
		      Thread.sleep(2000);
		      String error_msg=objContentDeveloper.Error_Msg();
		      Thread.sleep(2000);
			  if (error_msg.equals(objContentDeveloper.validation_msg1))
			     {
			     System.out.println("***Passed2!***:Single Content Developer Validation Method3 TestCase3 'Check required Question Data validation");
			     }
			  else
			     {
			     System.out.println("Failed!");
			     }
		      

		      
	    }
	      
	    @Test(priority=3)
			  //SingleContentDeveloper
	      public void Check_Question_Statement_Validation() throws InterruptedException, IOException {
				  
				  //Declaration
				  System.out.println("Single Content Developer Validation Method3 TestCase3 'Check required Question Data validation");
			      WebDriverWait wait = new WebDriverWait(driver,25000);
	 
			      //Back to home page
			      objContentDeveloper.Home_page();
			      Thread.sleep(600);
			      //Content Developer
			      objContentDeveloper.clickContentDeveloper();
			      objContentDeveloper.UnassignedQuestion();
			      WebElement subject=driver.findElement(objContentDeveloper.SubjectType);
			      Select ab= new Select(subject);
			      ab.selectByVisibleText("Mathematics");
			      objContentDeveloper.Subject_DropDownList();
			      String actual_id2="view"+objContentDeveloper.GENID();      	  	      
			      System.out.println(actual_id2);
			      objContentDeveloper.ViewGENID(actual_id2);
			      Thread.sleep(1000);
			      objContentDeveloper_Question = new ContentDeveloper_Question(driver);
			      String actual_id=objContentDeveloper_Question.SingleID();      	  	      
			      String titleId="title"+actual_id;
			      driver.findElement(By.id(titleId)).sendKeys("Test Title");  	        
			      //String QuestionId="question"+actual_id; //Check required question data validation
			      String KeyId="key"+actual_id;  
			      Thread.sleep(500);
			      //driver.findElement(By.id(QuestionId)).sendKeys("Test question data"); //Check required question data validation
			      driver.findElement(By.id(KeyId)).sendKeys("Test key value data");   
			      objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
			      objContentDeveloper_MetaDataLinking.MetaData_Linking();
			      Thread.sleep(500);
			      String oldTab = driver.getWindowHandle();
			      ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
			      newTab.remove(oldTab);
			      driver.switchTo().window(newTab.get(0));
			      objContentDeveloper_MetaDataLinking.Add_keyword_link();
			      Thread.sleep(500);
			      for ( String currentwindow : driver.getWindowHandles())driver.switchTo( ).window(currentwindow);
			                
			      {
				               Thread.sleep(400);
			                   Select dropdown=new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(objContentDeveloper_MetaDataLinking.KeyWordTypeID)));
			                   dropdown.selectByIndex(1);
			                   Thread.sleep(300);
			                   objContentDeveloper_MetaDataLinking.Assign_Keyword_Model();
			                   Thread.sleep(100);
			                   WebElement objKw=driver.findElement(objContentDeveloper.MetaDataModel);
			                   objKw.sendKeys("Nstest");
			                   objKw.sendKeys(Keys.ENTER);  
			                   Thread.sleep(300);
		                       objContentDeveloper.AddKeyword();
			                   Thread.sleep(300);
			                   driver.close();		                    
			       }  
			      
			      driver.switchTo().window(oldTab);
			      Thread.sleep(300);    
			      
			      //Optional note entry
			      String Note="Note"+actual_id;
		          objContentDeveloper.Note();
			 	  driver.findElement(By.id(Note)).sendKeys("Test developer note data");        
			 	  String addComment="addComment"+actual_id;
			      driver.findElement(By.id(addComment)).click();
			      Thread.sleep(200); 
			      objContentDeveloper.close_developer_note();
			 	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='btnSubmit']")));
			 	  objContentDeveloper.Save_Draft();//Save draft before check validation 
			      Thread.sleep(500);
			      driver.navigate().refresh();
			      Thread.sleep(1000);
			      objContentDeveloper.Distractor_creator();
			      Thread.sleep(2000);
			      String error_msg=objContentDeveloper.Error_Msg();
			      Thread.sleep(2000);
				  if (error_msg.equals(objContentDeveloper.validation_msg1))
				     {
				     System.out.println("***Passed3!***:Single Content Developer Validation Method3 TestCase3 'Check required Question Data validation");
				     }
				  else
				     {
				     System.out.println("Failed!");
				     }
				      
			     
				  

		    }
	    
	    @Test(priority=4)
		  //SingleContentDeveloper
		  public void Check_Question_Title_Validation() throws InterruptedException, IOException {
			  
			  //Declaration
			  System.out.println("Single Content Developer Validation Method4 TestCase4 'Check required Title validation");
		      WebDriverWait wait = new WebDriverWait(driver,25000);
		  	 
		      //Back to home page
		      objContentDeveloper.Home_page();
		      Thread.sleep(600);
		      
		      //Content Developer
		      objContentDeveloper.clickContentDeveloper();
		      objContentDeveloper.UnassignedQuestion();
		      WebElement subject=driver.findElement(objContentDeveloper.SubjectType);
		      Select ab= new Select(subject);
		      ab.selectByVisibleText("Mathematics");
		      objContentDeveloper.Subject_DropDownList();
		      String actual_id2="view"+objContentDeveloper.GENID();      	  	      
		      System.out.println(actual_id2);
		      objContentDeveloper.ViewGENID(actual_id2);
		      Thread.sleep(1000);
		      objContentDeveloper_Question = new ContentDeveloper_Question(driver);
		      String actual_id=objContentDeveloper_Question.SingleID();      	  	      
		      //String titleId="title"+actual_id;  //Check required Title data validation
		      //driver.findElement(By.id(titleId)).sendKeys("Test Title");  //Check required Title data validation	         
		      String QuestionId="question"+actual_id; //Check required question data validation
		      String KeyId="key"+actual_id;  
		      Thread.sleep(500);
		      driver.findElement(By.id(QuestionId)).sendKeys("Test question data"); //Check required question data validation
		      driver.findElement(By.id(KeyId)).sendKeys("Test key value data");   
		      objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
		      objContentDeveloper_MetaDataLinking.MetaData_Linking();
		      Thread.sleep(500);
		      String oldTab = driver.getWindowHandle();
		      ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		      newTab.remove(oldTab);
		      driver.switchTo().window(newTab.get(0));
		      objContentDeveloper_MetaDataLinking.Add_keyword_link();
		      Thread.sleep(500);
		      for ( String currentwindow : driver.getWindowHandles())driver.switchTo( ).window(currentwindow);
		                
		      {
			               Thread.sleep(400);
		                   Select dropdown=new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(objContentDeveloper_MetaDataLinking.KeyWordTypeID)));
		                   dropdown.selectByIndex(1);
		                   Thread.sleep(300);
		                   objContentDeveloper_MetaDataLinking.Assign_Keyword_Model();
		                   Thread.sleep(100);
		                   WebElement objKw=driver.findElement(objContentDeveloper.MetaDataModel);
		                   objKw.sendKeys("Nstest");
		                   objKw.sendKeys(Keys.ENTER);  
		                   Thread.sleep(300);
	                       objContentDeveloper.AddKeyword();
		                   Thread.sleep(300);
		                   driver.close();		                    
		       }  
		      
		      driver.switchTo().window(oldTab);
		      Thread.sleep(300);    
		      
		      //Optional note entry
		      String Note="Note"+actual_id;
	          objContentDeveloper.Note();
		 	  driver.findElement(By.id(Note)).sendKeys("Test developer note data");        
		 	  String addComment="addComment"+actual_id;
		      driver.findElement(By.id(addComment)).click();
		      Thread.sleep(200); 
		      objContentDeveloper.close_developer_note();
		 	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='btnSubmit']")));
		 	  objContentDeveloper.Save_Draft();//Save draft before check validation 
		      Thread.sleep(500);
		      driver.navigate().refresh();
		      Thread.sleep(1000);
		      objContentDeveloper.Distractor_creator();	
		      Thread.sleep(1000);
		      String error_msg=objContentDeveloper.Error_Msg();
			  if (error_msg.equals(objContentDeveloper.validation_msg1))
			     {
			     System.out.println("***Passed4***: Single Content Developer Validation Method4 TestCase4 'Check required Question Data validation");
			     }
			  else
			     {
			     System.out.println("Failed!");
			     }
			
		          
	
	    }
	    
	    @Test(priority=5)
		  //SingleContentDeveloper
		  public void Check_Question_Source_Validation() throws InterruptedException, IOException {
			  
			  //Declaration
			  System.out.println("Single Content Developer Validation Method5 TestCase5 'Check required Question source validation");
		      WebDriverWait wait = new WebDriverWait(driver,25000);
		 	 
		      //Back to home page
		      objContentDeveloper.Home_page();
		      Thread.sleep(600);
		      
		      //Content Developer
		      objContentDeveloper.clickContentDeveloper();
		      objContentDeveloper.UnassignedQuestion();
		      WebElement subject=driver.findElement(objContentDeveloper.SubjectType);
		      Select ab= new Select(subject);
		      ab.selectByVisibleText("Mathematics");
		      objContentDeveloper.Subject_DropDownList();
		      String actual_id2="view"+objContentDeveloper.GENID();      	  	      
		      System.out.println(actual_id2);
		      objContentDeveloper.ViewGENID(actual_id2);	
		      Thread.sleep(1000);
		      objContentDeveloper_Question = new ContentDeveloper_Question(driver);
		      String actual_id=objContentDeveloper_Question.SingleID();      	  	      
		      String titleId="title"+actual_id;  //Check required Title data validation
		      driver.findElement(By.id(titleId)).sendKeys("Test Title");  //Check required Title data validation	         
		      String QuestionId="question"+actual_id; //Check required question data validation
		      String KeyId="key"+actual_id;  
		      Thread.sleep(500);
		      driver.findElement(By.id(QuestionId)).sendKeys("Test question data"); //Check required question data validation
		      driver.findElement(By.id(KeyId)).sendKeys("Test key value data");   
		      objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
		      objContentDeveloper_MetaDataLinking.MetaData_Linking();
		      Thread.sleep(500);
		      String oldTab = driver.getWindowHandle();
		      ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		      newTab.remove(oldTab);
		      driver.switchTo().window(newTab.get(0));
		      objContentDeveloper_MetaDataLinking.Add_keyword_link();
		      Thread.sleep(500);
		      for ( String currentwindow : driver.getWindowHandles())driver.switchTo( ).window(currentwindow);
		                
		      {
			               Thread.sleep(400);
		                   Select dropdown=new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(objContentDeveloper_MetaDataLinking.KeyWordTypeID)));
		                   dropdown.selectByIndex(1);
		                   Thread.sleep(300);
		                   objContentDeveloper_MetaDataLinking.Assign_Keyword_Model();
		                   Thread.sleep(100);
		                   WebElement objKw=driver.findElement(objContentDeveloper.MetaDataModel);
		                   objKw.sendKeys("Nstest");
		                   objKw.sendKeys(Keys.ENTER);  
		                   Thread.sleep(300);
	                       objContentDeveloper.AddKeyword();
		                   Thread.sleep(300);
		                   driver.close();		                    
		       }  
		      
		      driver.switchTo().window(oldTab);
		      Thread.sleep(300);   		      
		      //Optional note entry
		      String Note="Note"+actual_id;
	          objContentDeveloper.Note();
		 	  driver.findElement(By.id(Note)).sendKeys("Test developer note data"); 
		 	  //QuestionSource access
		 	  String Question_Source2="questionSource"+actual_id;
		 	  WebElement Question_Source=driver.findElement(By.id(Question_Source2));
		      Select t= new Select(Question_Source);
		      t.selectByVisibleText("..Select..");
		 	  String addComment="addComment"+actual_id;
		      driver.findElement(By.id(addComment)).click();
		      Thread.sleep(200); 
		      objContentDeveloper.close_developer_note();
		      Thread.sleep(500); 
		 	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='btnSubmit']")));
		 	  objContentDeveloper.Save_Draft();//Save draft before check validation 
		      Thread.sleep(500);
		      driver.navigate().refresh();
		      Thread.sleep(1000);
		      objContentDeveloper.Distractor_creator();	
		      Thread.sleep(1000);
		      String error_msg=objContentDeveloper.Error_Msg();
			  if (error_msg.equals(objContentDeveloper.validation_msg1))
			     {
			     System.out.println("***Passed5!***:Single Content Developer Validation Method5 TestCase5 'Check required Question Source validation");
			     }
			  else
			     {
			     System.out.println("Failed!");
			     }
			
	
	    }
    
	    @Test(priority=6)
		  //SingleContentDeveloper
		  public void Check_Question_Locale_Validation() throws InterruptedException, IOException {
			  
			  //Declaration
			  System.out.println("Single Content Developer Validation Method6 TestCase6 'Check required Question source validation");
		      WebDriverWait wait = new WebDriverWait(driver,25000);
		 	 
		      //Back to home page
		      objContentDeveloper.Home_page();
		      Thread.sleep(300);
		      
		      //Content Developer
		      objContentDeveloper.clickContentDeveloper();
		      objContentDeveloper.UnassignedQuestion();
		      WebElement subject=driver.findElement(objContentDeveloper.SubjectType);
		      Select ab= new Select(subject);
		      ab.selectByVisibleText("Mathematics");
		      objContentDeveloper.Subject_DropDownList();
		      String actual_id2="view"+objContentDeveloper.GENID();      	  	      
		      System.out.println(actual_id2);
		      objContentDeveloper.ViewGENID(actual_id2);		      Thread.sleep(1000);
		      objContentDeveloper_Question = new ContentDeveloper_Question(driver);
		      String actual_id=objContentDeveloper_Question.SingleID();      	  	      
		      String titleId="title"+actual_id;  //Check required Title data validation
		      driver.findElement(By.id(titleId)).sendKeys("Test Title");  //Check required Title data validation	         
		      String QuestionId="question"+actual_id; //Check required question data validation
		      String KeyId="key"+actual_id;  
		      Thread.sleep(500);
		      driver.findElement(By.id(QuestionId)).sendKeys("Test question data"); //Check required question data validation
		      driver.findElement(By.id(KeyId)).sendKeys("Test key value data");   
		      objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
		      objContentDeveloper_MetaDataLinking.MetaData_Linking();
		      Thread.sleep(500);
		      String oldTab = driver.getWindowHandle();
		      ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		      newTab.remove(oldTab);
		      driver.switchTo().window(newTab.get(0));
		      objContentDeveloper_MetaDataLinking.Add_keyword_link();
		      Thread.sleep(500);
		      for ( String currentwindow : driver.getWindowHandles())driver.switchTo( ).window(currentwindow);
		                
		      {
			               Thread.sleep(400);
		                   Select dropdown=new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(objContentDeveloper_MetaDataLinking.KeyWordTypeID)));
		                   dropdown.selectByIndex(1);
		                   Thread.sleep(300);
		                   objContentDeveloper_MetaDataLinking.Assign_Keyword_Model();
		                   Thread.sleep(100);
		                   WebElement objKw=driver.findElement(objContentDeveloper.MetaDataModel);
		                   objKw.sendKeys("Nstest");
		                   objKw.sendKeys(Keys.ENTER);  
		                   Thread.sleep(300);
	                       objContentDeveloper.AddKeyword();
		                   Thread.sleep(300);
		                   driver.close();		                    
		       }  
		      
		      driver.switchTo().window(oldTab);
		      Thread.sleep(300);   		      
		      //Optional note entry
		      String Note="Note"+actual_id;
	          objContentDeveloper.Note();
		 	  driver.findElement(By.id(Note)).sendKeys("Test developer note data"); 
		 	  //Question locale locale424161083471
		 	  String locale2="locale"+actual_id;
		 	  WebElement locale=driver.findElement(By.id(locale2));
		      Select t= new Select(locale);
		      t.selectByVisibleText("..Select..");
		 	  String addComment="addComment"+actual_id;
		      driver.findElement(By.id(addComment)).click();
		      Thread.sleep(200); 
		      objContentDeveloper.close_developer_note();
		      Thread.sleep(500); 
		 	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='btnSubmit']")));
		 	  objContentDeveloper.Save_Draft();//Save draft before check validation 
		      Thread.sleep(500);
		      driver.navigate().refresh();
		      Thread.sleep(1000);
		      objContentDeveloper.Distractor_creator();	
		      Thread.sleep(1000);
		      String error_msg=objContentDeveloper.Error_Msg();
			  if (error_msg.equals(objContentDeveloper.validation_msg1))
			     {
			     System.out.println("***Passed6!***:Single Content Developer Validation Method6 TestCase6 'Check required Question Locale validation");
			     }
			  else
			     {
			     System.out.println("Failed!");
			     }
			
	
	    }

	    @Test(priority=7)
		  //SingleContentDeveloper
		  public void Check_Question_Keyword_Validation() throws InterruptedException, IOException {
	    	//Declaration
			  System.out.println("Single Content Developer Validation Method7 TestCase7 'Check required Question root keyword validation");
		      WebDriverWait wait = new WebDriverWait(driver,25000);  
		      
		      //Back to home page
		      objContentDeveloper.Home_page();
		      Thread.sleep(600);
		      
		      //Content Developer
		      objContentDeveloper.clickContentDeveloper();
		      objContentDeveloper.UnassignedQuestion();
		      WebElement subject=driver.findElement(objContentDeveloper.SubjectType);
		      Select ab= new Select(subject);
		      ab.selectByVisibleText("Mathematics");
		      objContentDeveloper.Subject_DropDownList();
		      String actual_id2="view"+objContentDeveloper.GENID();      	  	      
		      System.out.println(actual_id2);
		      objContentDeveloper.ViewGENID(actual_id2);	
		      Thread.sleep(1000);
		      objContentDeveloper_Question = new ContentDeveloper_Question(driver);
		      String actual_id=objContentDeveloper_Question.SingleID();      	  	      
		      String titleId="title"+actual_id;
		      driver.findElement(By.id(titleId)).sendKeys("Test Title");  	        
		      String QuestionId="question"+actual_id;
		      String KeyId="key"+actual_id;  //Check required Key value validation
		      Thread.sleep(500);
		      driver.findElement(By.id(QuestionId)).sendKeys("Test question data");
		      driver.findElement(By.id(KeyId)).sendKeys("Test key value data");  // Check required Key value validation
		      objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
		      objContentDeveloper_MetaDataLinking.MetaData_Linking();
		      Thread.sleep(500);
		      String oldTab = driver.getWindowHandle();
		      ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		      newTab.remove(oldTab);
		      driver.switchTo().window(newTab.get(0));
		      Thread.sleep(500);
		      for ( String currentwindow : driver.getWindowHandles())driver.switchTo( ).window(currentwindow);
		                
		       {
			               Thread.sleep(400);
			               objContentDeveloper_MetaDataLinking.DeleteKeyword();
	                    
		       }  
		      
		      driver.switchTo().window(oldTab);
		      Thread.sleep(300);    
		      
		      //Optional note entry
		      String Note="Note"+actual_id;
	          objContentDeveloper.Note();
		 	  driver.findElement(By.id(Note)).sendKeys("Test developer note data");        
		 	  String addComment="addComment"+actual_id;
		      driver.findElement(By.id(addComment)).click();
		      Thread.sleep(200); 
		      objContentDeveloper.close_developer_note();		      
		 	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='btnSubmit']")));
		 	  objContentDeveloper.Save_Draft();//Save draft before check validation 
		      Thread.sleep(500);
		      driver.navigate().refresh();
		      Thread.sleep(1000);
		      objContentDeveloper.Distractor_creator();
		      Thread.sleep(500);
		      String error_msg=objContentDeveloper.validation();
		      //System.out.println(error_msg);
			  if (error_msg.equals(objContentDeveloper.validation_msg2))
			     {
			     System.out.println("***Passed7!***:Single Content Developer Validation Method7 TestCase7 'Check required Question Keyword validation");
			     }
			  else
			     {
			     System.out.println("Failed!");
			     }
			
	
	    }

	    @AfterMethod 
		 public void takeScreenShotOnFailure(ITestResult testResult) throws IOException { 
		 	if (testResult.getStatus() == ITestResult.FAILURE) { 
		 		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); 
		 		FileUtils.copyFile(scrFile, new File("errorScreenshots\\" + testResult.getName() + "-" 
		 				+ Arrays.toString(testResult.getParameters()) +  ".jpg"));
		 	} 
		 }
	    
	    @AfterTest
	    
	    public void End(){

		    driver.close();
	    }

}
