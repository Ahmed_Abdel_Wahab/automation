package firsttestngpackage;

import static org.testng.Assert.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.record.PageBreakRecord.Break;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pom_pages.ContentDeveloper;
import pom_pages.ContentDeveloper_MetaDataLinking;
import pom_pages.ContentDeveloper_Question;
import pom_pages.DistractorEditor;
import pom_pages.EnglishCopyEditor;
import pom_pages.EnglishCopyEditorReviewer;
import pom_pages.KeywordSpecialist;
import pom_pages.LoginPage;
import pom_pages.SingleContentWriter;
import pom_pages.SingleMarkupSpecialist;
import pom_pages.SingleMathReviewer;
import pom_pages.Single_Markup_reviewer;

public class WorkSheet {

    WebDriver driver;
    LoginPage objLogin;
    ContentDeveloper objContentDeveloper;
    ContentDeveloper_Question objContentDeveloper_Question;
    ContentDeveloper_MetaDataLinking objContentDeveloper_MetaDataLinking;
    DistractorEditor objDistractorEditor;
    EnglishCopyEditor objEnglishCopyEditor;
    EnglishCopyEditorReviewer objEnglishCopyEditorReviewer;
    SingleMathReviewer objSingleMathReviewer;
    SingleMarkupSpecialist objSingleMarkupSpecialist;
    Single_Markup_reviewer objSingleMarkupreviewer;
    SingleContentWriter  objSingleContentWriter;
    KeywordSpecialist  objKeywordSpecialist;



    @BeforeTest

    public void setup() throws InterruptedException{

	    System.setProperty("webdriver.chrome.driver","Resources/chromedriver.exe");
	    ChromeOptions options = new ChromeOptions();
	    options.setHeadless(LoginPage.ChromeDriverOption);
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	    driver.get("http://testing.cds.nagwa.com");
	    driver.navigate().refresh();
	    Thread.sleep(400);

    }
	 @Test
	  //SingleContentDeveloper
	  public void SingleWorkSheetHappyPath() throws InterruptedException, IOException {
		  
		  //Declaration
	      WebDriverWait wait = new WebDriverWait(driver,25000);
		  objLogin = new LoginPage(driver);
	      objLogin.LoginToCDS("", "");
		  System.out.println("Single Workflow Happy Path for worksheets");
		  objContentDeveloper = new ContentDeveloper(driver);
		  objContentDeveloper_Question = new ContentDeveloper_Question(driver);
		  objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
		  objDistractorEditor = new DistractorEditor(driver);
		  objEnglishCopyEditor = new EnglishCopyEditor(driver);
		  objEnglishCopyEditorReviewer = new EnglishCopyEditorReviewer(driver);
		  objSingleMathReviewer = new SingleMathReviewer(driver);
		  objSingleMarkupSpecialist = new SingleMarkupSpecialist(driver);
		  objSingleMarkupreviewer = new Single_Markup_reviewer(driver);
		  objSingleContentWriter = new SingleContentWriter(driver);
		  objKeywordSpecialist = new KeywordSpecialist(driver);	
		  
	      //Content Developer
	      objContentDeveloper.clickContentDeveloper();
	      objContentDeveloper.Worksheet();
	      Thread.sleep(300);
	      String uniqueID = UUID.randomUUID().toString();
	      Thread.sleep(300);
	      objContentDeveloper.Worksheet_Name(uniqueID);
	      String WSName = "WorkSheet"+uniqueID;
	      String WSName2 = "WorkSheet"+" Name"+uniqueID+" 3 / 0 / 0";

	      Thread.sleep(300);
	      WebElement subject=driver.findElement(objContentDeveloper.WFSubjectID);
	      Select ab= new Select(subject);
	      ab.selectByVisibleText(objContentDeveloper.subject);
	      Thread.sleep(300);
	      objContentDeveloper.WorkSheet_Save();
	      Thread.sleep(2300);
	      driver.navigate().refresh();
	      Thread.sleep(2300);
	      String s=driver.getCurrentUrl();
	      String WorkSheetID = s.substring(72, 75);
		  System.out.println(WorkSheetID);
	      Thread.sleep(300);
	      //*[@id="singleId1"]
      
	      final String newLine1 = System.getProperty("line.separator");
	      try {
	          FileWriter writer = new FileWriter(objLogin.FilePath2, true);
	           
	          writer.write(newLine1 + WSName);
	          writer.write('\n');   
	          writer.close();
	      } catch (IOException e) {
	          e.printStackTrace();
	      }
	      Thread.sleep(500);
	      
	      try {
	          FileWriter writer = new FileWriter(objLogin.FilePath3, true);
	           
	          writer.write(newLine1 + WorkSheetID);
	          writer.write('\n');   
	          writer.close();
	      } catch (IOException e) {
	          e.printStackTrace();
	      }
	      Thread.sleep(500);
	      
	      
	      
	      String WorkSheetQ1= driver.findElement(objContentDeveloper.WorkSheetQ1).getText();
	      Thread.sleep(500);	    
		  System.out.println(WorkSheetQ1);
	      String WorkSheetQ2= driver.findElement(objContentDeveloper.WorkSheetQ2).getText();
	      Thread.sleep(300);
		  System.out.println(WorkSheetQ2);
	      Thread.sleep(1000);
	      
	      String Expand1= "expandQuestion"+WorkSheetQ1;
	      
	      String DistractorQ1_1= "distractor"+WorkSheetQ1+"0";
	      String DistractorQ1_2= "distractor"+WorkSheetQ1+"1";
	      String DistractorQ1_3= "distractor"+WorkSheetQ1+"2";
	      String DistractorQ1_4= "distractor"+WorkSheetQ1+"3";



	      String Expand2= "expandQuestion"+WorkSheetQ2;
	      
	      String DistractorQ2_1= "distractor"+WorkSheetQ2+"0";
	      String DistractorQ2_2= "distractor"+WorkSheetQ2+"1";
	      String DistractorQ2_3= "distractor"+WorkSheetQ2+"2";
	      String DistractorQ2_4= "distractor"+WorkSheetQ2+"3";


	      String AddDistractorQ1_1= "addDistractor"+WorkSheetQ1;
	      String AddDistractorQ1_2= "addDistractor"+WorkSheetQ1+"0";
	      String AddDistractorQ1_3= "addDistractor"+WorkSheetQ1+"1";
	      String AddDistractorQ1_4= "addDistractor"+WorkSheetQ1+"2";


	      String AddDistractorQ2_1= "addDistractor"+WorkSheetQ2;
	      String AddDistractorQ2_2= "addDistractor"+WorkSheetQ2+"0";
	      String AddDistractorQ2_3= "addDistractor"+WorkSheetQ2+"1";
	      String AddDistractorQ2_4= "addDistractor"+WorkSheetQ2+"2";

	      String AddQuestion2= "addQuestion"+WorkSheetQ2;

	      
	      driver.findElement(By.id(AddQuestion2)).click();
	      Thread.sleep(500);
	      
	      String WorkSheetQ3= driver.findElement(objContentDeveloper.WorkSheetQ3).getText();
	      
	      Thread.sleep(800);
		  System.out.println(WorkSheetQ3);

          String Expand3= "expandQuestion"+WorkSheetQ3;

	      String DistractorQ3_1= "distractor"+WorkSheetQ3+"0";
	      String DistractorQ3_2= "distractor"+WorkSheetQ3+"1";
	      String DistractorQ3_3= "distractor"+WorkSheetQ3+"2";
	      String DistractorQ3_4= "distractor"+WorkSheetQ3+"3";

	      
	      String AddDistractorQ3_1= "addDistractor"+WorkSheetQ3;
	      String AddDistractorQ3_2= "addDistractor"+WorkSheetQ3+"0";
	      String AddDistractorQ3_3= "addDistractor"+WorkSheetQ3+"1";
	      String AddDistractorQ3_4= "addDistractor"+WorkSheetQ3+"2";

	      
	      
	      driver.findElement(By.id(Expand1)).click();
	      Thread.sleep(500);
	      String titleId="title"+WorkSheetQ1;
	      driver.findElement(By.id(titleId)).sendKeys("Test Title");  	        
	      String QuestionId="question"+WorkSheetQ1;
	      String KeyId="key"+WorkSheetQ1;
	      Thread.sleep(500);
	      driver.findElement(By.id(QuestionId)).sendKeys("Test question data");
	      driver.findElement(By.id(KeyId)).sendKeys("Test key value data");
	      Thread.sleep(600);
	      driver.findElement(By.id(AddDistractorQ1_1)).click();
	      Thread.sleep(300);
	      driver.findElement(By.id(DistractorQ1_1)).sendKeys("test distractor 1");
	      Thread.sleep(300);
	      driver.findElement(By.id(AddDistractorQ1_2)).click();
	      Thread.sleep(300);
	      driver.findElement(By.id(DistractorQ1_2)).sendKeys("test distractor 2");
	      Thread.sleep(300);
	      driver.findElement(By.id(AddDistractorQ1_3)).click();
	      Thread.sleep(300);
	      driver.findElement(By.id(DistractorQ1_3)).sendKeys("test distractor 3");
	      Thread.sleep(300);
	      driver.findElement(By.id(AddDistractorQ1_4)).click();
	      Thread.sleep(300);
	      driver.findElement(By.id(DistractorQ1_4)).sendKeys("test distractor 4");
	      
	      Thread.sleep(900);	      
          driver.findElement(By.id(Expand2)).click();
	      Thread.sleep(300);
	      
	      String titleId2="title"+WorkSheetQ2;
	      driver.findElement(By.id(titleId2)).sendKeys("Test Title");  	        
	      String QuestionId2="question"+WorkSheetQ2;
	      String KeyId2="key"+WorkSheetQ2;
	      Thread.sleep(500);
	      driver.findElement(By.id(QuestionId2)).sendKeys("Test question data");
	      driver.findElement(By.id(KeyId2)).sendKeys("Test key value data");
	      //Now questions data are filled
	      Thread.sleep(300);
	      driver.findElement(By.id(AddDistractorQ2_1)).click();
	      Thread.sleep(300);
	      driver.findElement(By.id(DistractorQ2_1)).sendKeys("test distractor 1");
	      Thread.sleep(300);
	      driver.findElement(By.id(AddDistractorQ2_2)).click();
	      Thread.sleep(300);
	      driver.findElement(By.id(DistractorQ2_2)).sendKeys("test distractor 2");
	      Thread.sleep(300);
	      driver.findElement(By.id(AddDistractorQ2_3)).click();
	      Thread.sleep(300);
	      driver.findElement(By.id(DistractorQ2_3)).sendKeys("test distractor 3");
	      Thread.sleep(300);
	      driver.findElement(By.id(AddDistractorQ2_4)).click();
	      Thread.sleep(300);
	      driver.findElement(By.id(DistractorQ2_4)).sendKeys("test distractor 4");	      
	      Thread.sleep(900);
	      
	      
	      driver.findElement(By.id(Expand3)).click();
	      Thread.sleep(300);      
	      String titleId21="title"+WorkSheetQ3;
	      driver.findElement(By.id(titleId21)).sendKeys("Test Title");  	        
	      String QuestionId21="question"+WorkSheetQ3;
	      String KeyId21="key"+WorkSheetQ3;
	      Thread.sleep(500);
	      driver.findElement(By.id(QuestionId21)).sendKeys("Test question data");
	      driver.findElement(By.id(KeyId21)).sendKeys("Test key value data");
	      //Now questions data are filled
	      Thread.sleep(300);
	      driver.findElement(By.id(AddDistractorQ3_1)).click();
	      Thread.sleep(300);
	      driver.findElement(By.id(DistractorQ3_1)).sendKeys("test distractor 1");
	      Thread.sleep(300);
	      driver.findElement(By.id(AddDistractorQ3_2)).click();
	      Thread.sleep(300);
	      driver.findElement(By.id(DistractorQ3_2)).sendKeys("test distractor 2");
	      Thread.sleep(300);
	      driver.findElement(By.id(AddDistractorQ3_3)).click();
	      Thread.sleep(300);
	      driver.findElement(By.id(DistractorQ3_3)).sendKeys("test distractor 3");
	      Thread.sleep(300);
	      driver.findElement(By.id(AddDistractorQ3_4)).click();
	      Thread.sleep(300);
	      driver.findElement(By.id(DistractorQ3_4)).sendKeys("test distractor 4");
	      Thread.sleep(900);
	      objContentDeveloper.WS_Distractor();
	      Thread.sleep(1500);
	      objContentDeveloper.Home_page();
	      objContentDeveloper.Home_page();

	      //Distractor Creator
	      Thread.sleep(300);
	      objDistractorEditor.SingleDistractor();
	      Thread.sleep(300);
	      
	      WebElement subject3=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab3= new Select(subject3);
	      ab3.selectByVisibleText(objContentDeveloper.subject);
	      
	      //driver.findElement(objContentDeveloper.WorkSheetName).click();
	      WebElement Worksheet3=driver.findElement(objContentDeveloper.WorkSheetName);
	      Select ab111= new Select(Worksheet3);
	      ab111.selectByVisibleText(WSName2);	          
	      Thread.sleep(300);

	      driver.findElement(By.id("requestWorksheet")).click();  	    
	      Thread.sleep(500);
	      String viewID="view"+WorkSheetID;
	      driver.findElement(By.id(viewID)).click();  	
	      Thread.sleep(300);

	      String SubmitWS_Q1= "btnSubmit"+WorkSheetQ1;
	      String SubmitWS_Q2= "btnSubmit"+WorkSheetQ2;
	      String SubmitWS_Q3= "btnSubmit"+WorkSheetQ3;

	      
	      String ViewQID1= "view"+WorkSheetQ1;

	      String ViewQID2= "view"+WorkSheetQ2;

	      String ViewQID3= "view"+WorkSheetQ3;


	      driver.findElement(By.id(SubmitWS_Q1)).click(); 
	      Thread.sleep(1000);
	      driver.findElement(By.id(SubmitWS_Q2)).click();  
	      Thread.sleep(1000);
	      driver.findElement(By.id(SubmitWS_Q3)).click();  
	      
         objEnglishCopyEditor.Home_page();
	      
	      
		  //Third Role ' English Copy Editor ' ,assume same user have both roles'Navigate to Home page'		        
          objEnglishCopyEditor.EnglishCopyEditorDev();
	      WebElement subject4=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab4= new Select(subject4);
	      ab4.selectByVisibleText(objContentDeveloper.subject);		     	        
	      for(int x=0;x<5;x++)
	      {
	    	  objEnglishCopyEditor.RequestGENID();
	    	  
	      }
	      Thread.sleep(300);
	      driver.navigate().refresh();
	      Thread.sleep(1000);
	      objEnglishCopyEditor.ViewGENID(ViewQID1);
	      Thread.sleep(1000);
	      driver.navigate().refresh();
	      Thread.sleep(1000);
          objEnglishCopyEditor.EnglishCopyEditorReviewer();
	      Thread.sleep(500);
	      objDistractorEditor.ViewGENID(ViewQID2);
	      Thread.sleep(1000);
	      driver.navigate().refresh();
	      Thread.sleep(1000);
          objEnglishCopyEditor.EnglishCopyEditorReviewer();
	      Thread.sleep(500);
          objDistractorEditor.ViewGENID(ViewQID3);
	      Thread.sleep(1000);
	      driver.navigate().refresh();
	      Thread.sleep(1000);
          objEnglishCopyEditor.EnglishCopyEditorReviewer();
        //Fourth Role ' English copy editor reviewer' ,assume same user have both roles'Navigate to Home page'
	      //need to log out then login in different user
	      objEnglishCopyEditorReviewer.signout();
	      Thread.sleep(500);
	      objEnglishCopyEditorReviewer.LoginToCDS("", "");
	      Thread.sleep(500);	
	      objEnglishCopyEditorReviewer.English_copy_editor_reviewer();
	      //Select subject by visible text and request ID
	      WebElement subject5=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab5= new Select(subject5);
	      ab5.selectByVisibleText(objContentDeveloper.subject);
	      for(int x=0;x<5;x++)
	      {
	    	  objEnglishCopyEditorReviewer.RequestGENID();
	    	  
	      }
	      Thread.sleep(300);
	      driver.navigate().refresh();
	      objEnglishCopyEditorReviewer.ViewGENID(ViewQID1);
	      Thread.sleep(800);
	      wait.until(ExpectedConditions.visibilityOfElementLocated(objEnglishCopyEditorReviewer.Submit_To_MathReviewer));
	      objEnglishCopyEditorReviewer.Submit_To_MathReviewer();
	      Thread.sleep(800);
	      objEnglishCopyEditorReviewer.ViewGENID(ViewQID2);
	      Thread.sleep(800);
	      wait.until(ExpectedConditions.visibilityOfElementLocated(objEnglishCopyEditorReviewer.Submit_To_MathReviewer));
	      objEnglishCopyEditorReviewer.Submit_To_MathReviewer();
	      Thread.sleep(500);
	      objEnglishCopyEditorReviewer.ViewGENID(ViewQID3);
	      Thread.sleep(800);
	      wait.until(ExpectedConditions.visibilityOfElementLocated(objEnglishCopyEditorReviewer.Submit_To_MathReviewer));
	      objEnglishCopyEditorReviewer.Submit_To_MathReviewer();
	      Thread.sleep(1500);

	      //Fifth Role ' Math reviewer' ,assume same user have both roles'Navigate to Home page'
	      objEnglishCopyEditorReviewer.signout();
	      Thread.sleep(500);
	      objLogin.LoginToCDS("", "");
	      Thread.sleep(200);	
	      objSingleMathReviewer.Submit_To_MathReviewer();
	      WebElement subject7=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab7= new Select(subject7);
	      ab7.selectByVisibleText(objContentDeveloper.subject);
	      
	      WebElement Worksheet2=driver.findElement(objContentDeveloper.WorkSheetName);
	      Select ab8= new Select(Worksheet2);
	      ab8.selectByVisibleText(WSName2);	      
	      
	      driver.findElement(By.id("requestWorksheet")).click();  	    
	      
	      driver.findElement(By.id(viewID)).click();  	
	      Thread.sleep(300);

	      String ApproveQ1="btnApprove"+WorkSheetQ1;
	      String ApproveQ2="btnApprove"+WorkSheetQ2;
	      String ApproveQ3="btnApprove"+WorkSheetQ3;

	      Thread.sleep(500);

	      driver.findElement(By.id(ApproveQ1)).click();
	      Thread.sleep(500);
	      driver.findElement(By.id(ApproveQ2)).click();
	      Thread.sleep(500);
	      driver.findElement(By.id(ApproveQ3)).click();
	      Thread.sleep(800);

	      objEnglishCopyEditor.Home_page();

	       //Fifth role 'Single Markup specialist 	      
	      //Now we get ID of the single id 'old one' from txt file
	      final FileReader namereader = new FileReader(objSingleMarkupSpecialist.OldIdSingle);
	      final BufferedReader in = new BufferedReader(namereader);
	      String oldID=in.readLine();    
	      System.out.println("old ID"+ oldID);
	      //execute rename oldid* actualID'New id' *
	      try {
	          String new_dir=objSingleMarkupSpecialist.GENID_Folder_Single;
	          Runtime rt = Runtime.getRuntime();		            
	         
				String newID=WorkSheetQ3;
				String cmd = "cmd.exe /c ren" + " " +oldID+"*"+" "+newID+"*" ;
				
	          rt.exec(cmd.toString(), null, new File(new_dir));
		      System.out.println("command"+ cmd);

	  		} catch (IOException e)
	              {
	  		        System.out.println("FROM CATCH" + e.toString());
	  		    }
	      //overwrite oldid with newid in txt file
	       Thread.sleep(500);
	       FileWriter writer = new FileWriter(objSingleMarkupSpecialist.OldIdSingle, false);      
	       writer.write(WorkSheetQ3);
	       writer.close();
	       System.out.println(oldID);
	       System.out.println(WorkSheetQ3);	       
	       Thread.sleep(1000);
	       String xml=objSingleMarkupSpecialist.GENID_Folder_Single+WorkSheetQ3+".single.xml";
	       File inputXML = new File(xml);
	       FileReader fr = new FileReader(inputXML);
	       String s1= "";   
	       StringBuilder strTotale = new StringBuilder();
	       BufferedReader br = new BufferedReader(fr);
	       try {
	           while ((s1 = br.readLine()) != null) {
	        	   s1=s1.replaceAll(oldID, WorkSheetQ3);
	        	   strTotale.append(s1);
	        	   }
	       } catch ( IOException  e) {
	           e.printStackTrace();
	       }  
	       finally
	       {
	               try {
	                   br.close();
	               } catch (IOException e) {
	                   e.printStackTrace();
	               }
	       }
	       PrintWriter out = new PrintWriter(xml);       
	       out.println(strTotale);
	       out.close();
	      //Xml content
	      System.out.println(strTotale.toString()); 	      
	      objSingleMarkupSpecialist.Home_page();
	      Thread.sleep(200);
	      objSingleMarkupSpecialist.SingleMarkupSpecialistDev();
	      driver.navigate().refresh();
	      Thread.sleep(500);
	      WebElement subject6=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab6= new Select(subject6);
	      ab6.selectByVisibleText(objContentDeveloper.subject);
	      for(int x=0;x<4;x++)
	      {
	    	  objSingleMarkupSpecialist.RequestGENID();
	    	  
	      }
	      Thread.sleep(3000);
	      driver.navigate().refresh();
	      Thread.sleep(500);
	      driver.navigate().refresh();
	      Thread.sleep(500);

	      WebElement element = driver.findElement(By.id("file"));
	      element.sendKeys(xml);	          
	      String Status=null;
	      Thread.sleep(8000); 
	      Status=objSingleMarkupSpecialist.Status();
	      System.out.println(Status.toString());
	      driver.navigate().refresh();
	      Thread.sleep(500);
	      driver.navigate().refresh();
	       while (Status.contains("Processing"))
	      {
		      Thread.sleep(6000);
		      driver.navigate().refresh();
		      Thread.sleep(2000);
		      Status=objSingleMarkupSpecialist.Status();
		      Thread.sleep(2000);
		      //Check ID status
		      //System.out.println(Status.toString());
	      }
	       if(Status.contains("Failed"))
	       {
	    	   
	 	      System.out.println("IB tool status if failure and test case is failed");
	    	   fail("IB tool status if failure and test case is failed");
	    	   
	       }
	      objSingleMarkupSpecialist.ViewGENID(ViewQID3);
	      Thread.sleep(500);
	      driver.navigate().refresh();
	      Thread.sleep(1500);
	      objSingleMarkupSpecialist.Submit_To_MarkupReviewer();
	      System.out.println(Status.toString()); 
	      Thread.sleep(2000);	
	      driver.navigate().refresh();
	      Thread.sleep(2000);	

	      final FileReader namereader1 = new FileReader(objSingleMarkupSpecialist.OldIdSingle);
	      final BufferedReader in1 = new BufferedReader(namereader1);
	      String oldID1=in1.readLine();    
	      //execute rename oldid* actualID'New id' *
	      try {
	          String new_dir=objSingleMarkupSpecialist.GENID_Folder_Single;
	          Runtime rt = Runtime.getRuntime();		            
	         
				String newID=WorkSheetQ2;
				String cmd = "cmd.exe /c ren" + " " +oldID1+"*"+" "+newID+"*" ;
	          rt.exec(cmd.toString(), null, new File(new_dir));
	  		} catch (IOException e)
	              {
	  		        System.out.println("FROM CATCH" + e.toString());
	  		    }
	      //overwrite oldid with newid in txt file
	       Thread.sleep(500);
	       FileWriter writer1 = new FileWriter(objSingleMarkupSpecialist.OldIdSingle, false);      
	       writer1.write(WorkSheetQ2);
	       writer1.close();
	       System.out.println(oldID1);
	       System.out.println(WorkSheetQ2);	       
	       Thread.sleep(1000);
	       String xml1=objSingleMarkupSpecialist.GENID_Folder_Single+WorkSheetQ2+".single.xml";
	       File inputXML1 = new File(xml1);
	       FileReader fr1 = new FileReader(inputXML1);
	       String s21= "";   
	       StringBuilder strTotale1 = new StringBuilder();
	       BufferedReader br1 = new BufferedReader(fr1);
	       try {
	           while ((s21 = br1.readLine()) != null) {
	        	   s21=s21.replaceAll(oldID1, WorkSheetQ2);
	        	   strTotale1.append(s21);
	        	   }
	       } catch ( IOException  e) {
	           e.printStackTrace();
	       }  
	       finally
	       {
	               try {
	                   br1.close();
	               } catch (IOException e) {
	                   e.printStackTrace();
	               }
	       }
	       PrintWriter out1 = new PrintWriter(xml1);       
	       out1.println(strTotale1);
	       out1.close();
	      //Xml content
	      System.out.println(strTotale1.toString()); 	      
	      Thread.sleep(500);
	      driver.navigate().refresh();
	      WebElement element1 = driver.findElement(By.id("file"));
	      element1.sendKeys(xml1);	          
	      String Status1=null;
	      Thread.sleep(8000); 
	      Status1=objSingleMarkupSpecialist.Status();
	      System.out.println(Status1.toString());
	      driver.navigate().refresh();
	      Thread.sleep(500);
	      driver.navigate().refresh();
	       while (Status1.contains("Processing"))
	      {
		      Thread.sleep(6000);
		      driver.navigate().refresh();
		      Thread.sleep(2000);
		      Status1=objSingleMarkupSpecialist.Status();
		      Thread.sleep(2000);
		      //Check ID status
		      //System.out.println(Status.toString());
	      }
	      objSingleMarkupSpecialist.ViewGENID(ViewQID2);
	      Thread.sleep(500);
	      driver.navigate().refresh();
	      Thread.sleep(1500);
	      objSingleMarkupSpecialist.Submit_To_MarkupReviewer();
	      System.out.println(Status1.toString()); 
	      Thread.sleep(1000);	
	      final FileReader namereader11 = new FileReader(objSingleMarkupSpecialist.OldIdSingle);
	      final BufferedReader in11 = new BufferedReader(namereader11);
	      String oldID11=in11.readLine();    
	      System.out.println("old ID"+ oldID11);
	      //execute rename oldid* actualID'New id' *
	      try {
	          String new_dir=objSingleMarkupSpecialist.GENID_Folder_Single;
	          Runtime rt = Runtime.getRuntime();		            
	         
				String newID=WorkSheetQ1;
				String cmd = "cmd.exe /c ren" + " " +oldID11+"*"+" "+newID+"*" ;
				
	          rt.exec(cmd.toString(), null, new File(new_dir));
		      System.out.println("command"+ cmd);

	  		} catch (IOException e)
	              {
	  		        System.out.println("FROM CATCH" + e.toString());
	  		    }
	      //overwrite oldid with newid in txt file
	       Thread.sleep(500);
	       FileWriter writer11 = new FileWriter(objSingleMarkupSpecialist.OldIdSingle, false);      
	       writer11.write(WorkSheetQ1);
	       writer11.close();
	       System.out.println(oldID11);
	       System.out.println(WorkSheetQ1);	       
	       Thread.sleep(1000);
	       String xml11=objSingleMarkupSpecialist.GENID_Folder_Single+WorkSheetQ1+".single.xml";
	       File inputXML11 = new File(xml11);
	       FileReader fr11 = new FileReader(inputXML11);
	       String s11= "";   
	       StringBuilder strTotale11 = new StringBuilder();
	       BufferedReader br11 = new BufferedReader(fr11);
	       try {
	           while ((s11 = br11.readLine()) != null) {
	        	   s11=s11.replaceAll(oldID11, WorkSheetQ1);
	        	   strTotale11.append(s11);
	        	   }
	       } catch ( IOException  e) {
	           e.printStackTrace();
	       }  
	       finally
	       {
	               try {
	                   br11.close();
	               } catch (IOException e) {
	                   e.printStackTrace();
	               }
	       }
	       PrintWriter out11 = new PrintWriter(xml11);       
	       out11.println(strTotale11);
	       out11.close();
	      //Xml content
	      System.out.println(strTotale11.toString()); 	      
	      driver.navigate().refresh();
	      Thread.sleep(500);

	      WebElement element11 = driver.findElement(By.id("file"));
	      element11.sendKeys(xml11);	          
	      String Status11=null;
	      Thread.sleep(8000); 
	      Status11=objSingleMarkupSpecialist.Status();
	      System.out.println(Status11.toString());
	      driver.navigate().refresh();
	      Thread.sleep(500);
	      driver.navigate().refresh();
	       while (Status11.contains("Processing"))
	      {
		      Thread.sleep(6000);
		      driver.navigate().refresh();
		      Thread.sleep(2000);
		      Status11=objSingleMarkupSpecialist.Status();
		      Thread.sleep(2000);
		      //Check ID status
		      //System.out.println(Status.toString());
	      }
	      objSingleMarkupSpecialist.ViewGENID(ViewQID1);
	      Thread.sleep(500);
	      driver.navigate().refresh();
	      Thread.sleep(1500);
	      objSingleMarkupSpecialist.Submit_To_MarkupReviewer();
	      System.out.println(Status11.toString()); 
	      Thread.sleep(2000);
	      //Markup Reviewer
	      objSingleMarkupreviewer.Home_page();
	      Thread.sleep(200);
	      objSingleMarkupreviewer.SingleMarkupReviewerDev();
	      Thread.sleep(200);
	      WebElement subject10=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab10= new Select(subject10);
	      ab10.selectByVisibleText(objContentDeveloper.subject);
	      for(int x=0;x<4;x++)
	      {
	    	  objSingleMarkupreviewer.RequestGENID();
	    	  
	      }

	      Thread.sleep(1000);
	      objSingleMarkupreviewer.ViewGENID(ViewQID1);
	      Thread.sleep(2000);
	      objSingleMarkupreviewer.Submit_To_ContentWriter();
	      Thread.sleep(1000);
	      objSingleMarkupreviewer.ViewGENID(ViewQID2);
	      Thread.sleep(2000);
	      objSingleMarkupreviewer.Submit_To_ContentWriter();
	      Thread.sleep(1000);
	      objSingleMarkupreviewer.ViewGENID(ViewQID3);
	      Thread.sleep(2000);
	      objSingleMarkupreviewer.Submit_To_ContentWriter();
	      Thread.sleep(2000);
	      objSingleContentWriter.Home_page();
	      Thread.sleep(200);
	      //Content Writer 
	      objSingleContentWriter.SingleContentWriterDev();
	      Thread.sleep(300);
	      
	      WebElement subject31=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab31= new Select(subject31);
	      ab31.selectByVisibleText(objContentDeveloper.subject);
	      
	      WebElement Worksheet1=driver.findElement(objContentDeveloper.WorkSheetName);
	      Select ab21= new Select(Worksheet1);
	      ab21.selectByVisibleText(WSName2);	      
	      
	      driver.findElement(By.id("requestWorksheet")).click();  	    
	      Thread.sleep(300);
	      driver.findElement(By.id(viewID)).click();  	
	      Thread.sleep(300);

	      driver.findElement(By.id(ApproveQ1)).click();
	      Thread.sleep(500);
	      driver.findElement(By.id(ApproveQ2)).click();
	      Thread.sleep(500);
	      driver.findElement(By.id(ApproveQ3)).click();
	      Thread.sleep(1000);
	      objSingleContentWriter.Home_page();
	      Thread.sleep(200);
	      objKeywordSpecialist.KeywordSpecialistDev();
	      Thread.sleep(4000);
	      
	      objKeywordSpecialist.ChooseKeywordWS();
	      Thread.sleep(600);
	      String edit="edit"+WorkSheetQ1;
	      String approve="approve"+WorkSheetQ1;
	      //Creating our own Path to access elements as IDs of elements aren't fixed and unique per ID and can't be included into POM
	      //*[@id="single486169485643"]/div[3]/a
	      Thread.sleep(3000);
	      driver.navigate().refresh();
	      Thread.sleep(6000);
	      //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath1))).click();
	      objKeywordSpecialist.edit(edit);
	      Thread.sleep(5000);
	      driver.navigate().refresh();
	      Thread.sleep(7000);
	      //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath2))).click();
	      //driver.findElement(By.id(approve)).click();
	      objKeywordSpecialist.Approve(approve);
	      /////////Print
	      System.out.println("Aprroved ID is "+WorkSheetQ1); 
	      Thread.sleep(600);
	      String edit1="edit"+WorkSheetQ2;
	      String approve1="approve"+WorkSheetQ2;
	      //Creating our own Path to access elements as IDs of elements aren't fixed and unique per ID and can't be included into POM
	      //*[@id="single486169485643"]/div[3]/a
	      Thread.sleep(3000);
	      driver.navigate().refresh();
	      Thread.sleep(6000);
	      //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath1))).click();
	      objKeywordSpecialist.edit(edit1);
	      Thread.sleep(5000);
	      driver.navigate().refresh();
	      Thread.sleep(9000);
	      //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath2))).click();
	      //driver.findElement(By.id(approve)).click();
	      objKeywordSpecialist.Approve(approve1);
	      /////////Print
	      System.out.println("Aprroved ID is "+WorkSheetQ2);
	      Thread.sleep(600);
	      String edit11="edit"+WorkSheetQ3;
	      String approve11="approve"+WorkSheetQ3;
	      //Creating our own Path to access elements as IDs of elements aren't fixed and unique per ID and can't be included into POM
	      //*[@id="single486169485643"]/div[3]/a
	      Thread.sleep(3000);
	      driver.navigate().refresh();
	      Thread.sleep(6000);
	      //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath1))).click();
	      objKeywordSpecialist.edit(edit11);
	      Thread.sleep(5000);
	      driver.navigate().refresh();
	      Thread.sleep(9000);
	      //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath2))).click();
	      //driver.findElement(By.id(approve)).click();
	      objKeywordSpecialist.Approve(approve11);
	      /////////Print
	      System.out.println("Aprroved ID is "+WorkSheetQ3);
	      System.out.println("Aprroved ID Keyword is Mathematics"); 
	    
	      
	      

		  
		  
	 }	  
	 
	 
	 @AfterMethod 
	 public void takeScreenShotOnFailure(ITestResult testResult) throws IOException, InterruptedException { 
	 	if (testResult.getStatus() == ITestResult.FAILURE) { 
	 		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); 
	 		FileUtils.copyFile(scrFile, new File("errorScreenshots\\" + testResult.getName() + "-" 
	 				+ Arrays.toString(testResult.getParameters()) +  ".jpg"));
	 	}
	 	
	 Thread.sleep(1000);
	 }
	   @AfterTest
	    
	 public void End(){

		    driver.close();
	    }


}

