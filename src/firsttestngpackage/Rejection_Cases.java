package firsttestngpackage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pom_pages.ContentDeveloper;
import pom_pages.ContentDeveloper_MetaDataLinking;
import pom_pages.ContentDeveloper_Question;
import pom_pages.DistractorEditor;
import pom_pages.EnglishCopyEditor;
import pom_pages.EnglishCopyEditorReviewer;
import pom_pages.LoginPage;
import pom_pages.SingleContentWriter;
import pom_pages.SingleMarkupSpecialist;
import pom_pages.SingleMathReviewer;
import pom_pages.Single_Markup_reviewer;
import org.openqa.selenium.JavascriptExecutor;

public class Rejection_Cases {

	WebDriver driver;
    LoginPage objLogin;
    ContentDeveloper objContentDeveloper;
    ContentDeveloper_Question objContentDeveloper_Question;
    ContentDeveloper_MetaDataLinking objContentDeveloper_MetaDataLinking;
    DistractorEditor objDistractorEditor;
    EnglishCopyEditor objEnglishCopyEditor;
    SingleMathReviewer objSingleMathReviewer;
    SingleMarkupSpecialist objSingleMarkupSpecialist;
    EnglishCopyEditorReviewer objEnglishCopyEditorReviewer;
    Single_Markup_reviewer objSingleMarkupreviewer;
    SingleContentWriter  objSingleContentWriter;


    
    
    @BeforeTest

    public void setup() throws InterruptedException{

	    System.setProperty("webdriver.chrome.driver","Resources/chromedriver.exe");
	    ChromeOptions options = new ChromeOptions();
	    options.setHeadless(LoginPage.ChromeDriverOption);
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	    driver.get("http://testing.cds.nagwa.com");
	    driver.navigate().refresh();
	    Thread.sleep(400);
		objLogin = new LoginPage(driver);
	    objLogin.LoginToCDS("", "");
		objContentDeveloper = new ContentDeveloper(driver);
		objContentDeveloper_Question = new ContentDeveloper_Question(driver);
		objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
		objDistractorEditor = new DistractorEditor(driver);
		objEnglishCopyEditor = new EnglishCopyEditor(driver);
		objEnglishCopyEditorReviewer = new EnglishCopyEditorReviewer(driver);
		objSingleMathReviewer = new SingleMathReviewer(driver);
		objSingleMarkupSpecialist = new SingleMarkupSpecialist(driver);
		objSingleMarkupreviewer = new Single_Markup_reviewer(driver);
		objSingleContentWriter = new SingleContentWriter(driver);



    }

    
    @Test(priority=1)
	  //SingleContentDeveloper
	  public void Rejection_From_Single_MathReviewer_To_ContentDeveloper() throws InterruptedException, IOException {	
  	
		  System.out.println("Rejection_From_Single_MathReviewer_To_ContentDeveloper TestCase1");
  	
  	  WebDriverWait wait = new WebDriverWait(driver,25000);	    	
  	//Content Developer
      objContentDeveloper.clickContentDeveloper();
      objContentDeveloper.UnassignedQuestion();
      WebElement subject=driver.findElement(objContentDeveloper.SubjectType);
      Select ab= new Select(subject);
      ab.selectByVisibleText(objContentDeveloper.subject);
      objContentDeveloper.Subject_DropDownList();
      String actual_id2="view"+objContentDeveloper.GENID();      	  	      
      System.out.println(actual_id2);
      objContentDeveloper.ViewGENID(actual_id2);
      Thread.sleep(1000);
      objContentDeveloper_Question = new ContentDeveloper_Question(driver);
      String actual_id=objContentDeveloper_Question.SingleID();      	  	      
      String titleId="title"+actual_id;
      String ViewId="view"+actual_id;
      driver.findElement(By.id(titleId)).sendKeys("Test Title");  	        
      String QuestionId="question"+actual_id;
      String KeyId="key"+actual_id;
      Thread.sleep(500);
      driver.findElement(By.id(QuestionId)).sendKeys("Test question data");
      driver.findElement(By.id(KeyId)).sendKeys("Test key value data");
      objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
      objContentDeveloper_MetaDataLinking.MetaData_Linking();
      Thread.sleep(500);
      String oldTab = driver.getWindowHandle();
      ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
      newTab.remove(oldTab);
      driver.switchTo().window(newTab.get(0));
      objContentDeveloper_MetaDataLinking.Add_keyword_link();
      Thread.sleep(500);
      for ( String currentwindow : driver.getWindowHandles())driver.switchTo( ).window(currentwindow);
                 {
	               Thread.sleep(400);
                   Select dropdown=new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(objContentDeveloper_MetaDataLinking.KeyWordTypeID)));
                   dropdown.selectByIndex(1);
                   Thread.sleep(300);
                   objContentDeveloper_MetaDataLinking.Assign_Keyword_Model();
                   Thread.sleep(100);
                   WebElement objKw=driver.findElement(objContentDeveloper.MetaDataModel);
                   objKw.sendKeys("Nstest");
                   objKw.sendKeys(Keys.ENTER);  
                   Thread.sleep(300);
                   objContentDeveloper.AddKeyword();
                   Thread.sleep(300);
                   driver.close();		                    
                 }   		           
      driver.switchTo().window(oldTab);
      Thread.sleep(300);           
      String Note="Note"+actual_id;
      objContentDeveloper.Note();
 	  driver.findElement(By.id(Note)).sendKeys("Test developer note data");        
 	  String addComment="addComment"+actual_id;
      driver.findElement(By.id(addComment)).click();
      Thread.sleep(200); 
      objContentDeveloper.close_developer_note();
 	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='btnSubmit']")));
      objContentDeveloper.Distractor_creator();
      Thread.sleep(500);		    
      try {
      	   Thread.sleep(300);
          }catch (InterruptedException e)	 {
	        String error_msg=objContentDeveloper.Error_Msg();
	        String error_msg2=objContentDeveloper.validation();
	        if (error_msg.equals(objContentDeveloper.validation_msg1))
	        {
	            System.out.println("Fail 'incomplete data'");
	        }
	        else if (error_msg2.equals(objContentDeveloper.validation_msg2))
	        {
	            System.out.println("Fail 'No Root Keyword'");
	        }
	        else
	        {
	            System.out.println(objContentDeveloper.validation_msg3);
	        }
      
        }
      final String newLine1 = System.getProperty("line.separator");
      try {
          FileWriter writer = new FileWriter(objLogin.FilePath, true);
          
          writer.write(newLine1 + actual_id);
          writer.write('\n');   
          writer.close();
      } catch (IOException e) {
          e.printStackTrace();
      }
      Thread.sleep(500);
      
      
      //Second Role ' Distractor creator' ,assume same user have both roles'Navigate to Home page'		        
      objContentDeveloper.Home_page();
      Thread.sleep(200);
      objDistractorEditor.SingleDistractor();
      Thread.sleep(300);
      objDistractorEditor.unassigned_question();
      WebElement subject2=driver.findElement(objContentDeveloper.SubjectType);
      Select ab2= new Select(subject2);
      ab2.selectByVisibleText(objContentDeveloper.subject);
      Thread.sleep(200);	      
      for(int x=0;x<4;x++)
      {
    	  objDistractorEditor.RequestGENID();
    	  
      }
      Thread.sleep(300);
      driver.navigate().refresh();
      objDistractorEditor.ViewGENID(ViewId); 
      Thread.sleep(600);
      objDistractorEditor.Submit_to_copyEditor();
      objEnglishCopyEditor.Home_page();
      
      
	  //Third Role ' English Copy Editor ' ,assume same user have both roles'Navigate to Home page'		        
      objEnglishCopyEditor.EnglishCopyEditorDev();
      WebElement subject3=driver.findElement(objContentDeveloper.SubjectType);
      Select ab3= new Select(subject3);
      ab3.selectByVisibleText(objContentDeveloper.subject);		     	        
      for(int x=0;x<4;x++)
      {
    	  objEnglishCopyEditor.RequestGENID();
    	  
      }
      Thread.sleep(300);
      driver.navigate().refresh();
      Thread.sleep(300);
      objEnglishCopyEditor.ViewGENID(ViewId);
      Thread.sleep(900);
      objEnglishCopyEditor.EnglishCopyEditorReviewer();
      Thread.sleep(500);
      
      
      //Fourth Role ' English copy editor reviewer' ,assume same user have both roles'Navigate to Home page'
      //need to log out then login in different user
      objEnglishCopyEditorReviewer.signout();
      Thread.sleep(500);
      objEnglishCopyEditorReviewer.LoginToCDS("", "");
      Thread.sleep(500);	
      objEnglishCopyEditorReviewer.English_copy_editor_reviewer();
      //Select subject by visible text and request ID
      WebElement subject4=driver.findElement(objContentDeveloper.SubjectType);
      Select ab4= new Select(subject4);
      ab4.selectByVisibleText(objContentDeveloper.subject);
      for(int x=0;x<4;x++)
      {
    	  objEnglishCopyEditorReviewer.RequestGENID();
    	  
      }

      Thread.sleep(1000);
      driver.navigate().refresh();
      objEnglishCopyEditorReviewer.ViewGENID(ViewId);
      Thread.sleep(800);
      wait.until(ExpectedConditions.visibilityOfElementLocated(objEnglishCopyEditorReviewer.Submit_To_MathReviewer));
      objEnglishCopyEditorReviewer.Submit_To_MathReviewer();
      Thread.sleep(500);
      
      
      //Fifth Role ' Math reviewer' ,assume same user have both roles'Navigate to Home page'
      objEnglishCopyEditorReviewer.signout();
      Thread.sleep(1000);
      objLogin.LoginToCDS("", "");
      Thread.sleep(500);	
      objSingleMathReviewer.Submit_To_MathReviewer();
      Thread.sleep(500);
      objSingleMathReviewer.UnassignedQuestion();
      WebElement subject5=driver.findElement(objContentDeveloper.SubjectType);
      Select ab5= new Select(subject5);
      ab5.selectByVisibleText(objContentDeveloper.subject);
      for(int x=0;x<4;x++)
      {
    	  objSingleMathReviewer.RequestGENID();
    	  
      }

      Thread.sleep(1000);
      driver.navigate().refresh();	
      objSingleMathReviewer.ViewGENID(ViewId);
      Thread.sleep(1000);
      objSingleMathReviewer.Comment();
      Thread.sleep(1000);
      objSingleMathReviewer.Reject_To_ContentDevloper();	
      Thread.sleep(1000);
      
      
      //Signout then access content developer to make sure rejected question back 
      objEnglishCopyEditorReviewer.signout();
      Thread.sleep(500);
      objLogin.LoginToCDS("", "");
      Thread.sleep(500);
      objContentDeveloper.clickContentDeveloper();
      objContentDeveloper.UnassignedQuestion();
      driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
      boolean exists = driver.findElements(By.id(ViewId)).size() != 0;
      Thread.sleep(300);
	  if (exists)
	     {
		  System.out.println("**PASSED**:Rejection_From_Single_MathReviewer_To_ContentDeveloper TestCase1");

	     }
	  else
	     {
		  System.out.println("Failed!!");

	     }
      Thread.sleep(500);
  }
    
    
    
    @Test(priority=2)
 	  //SingleContentDeveloper
 	  public void Rejection_From_Single_MarkupReviewer_To_MarkupSpecialist() throws InterruptedException, IOException {	
   	
 		  System.out.println("Rejection_From_Single_MarkupReviewer_To_MarkupSpecialist TestCase2");
   	
   	  WebDriverWait wait = new WebDriverWait(driver,25000);	    	
   	//Content Developer
       objSingleMarkupSpecialist.Home_page();
       Thread.sleep(300);
       objContentDeveloper.clickContentDeveloper();
	      objContentDeveloper.UnassignedQuestion();
	      WebElement subject=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab= new Select(subject);
	      ab.selectByVisibleText(objContentDeveloper.subject);
	      objContentDeveloper.Subject_DropDownList();
	      String actual_id2="view"+objContentDeveloper.GENID();      	  	      
	      System.out.println(actual_id2);
	      objContentDeveloper.ViewGENID(actual_id2);
	      Thread.sleep(1000);
	      objContentDeveloper_Question = new ContentDeveloper_Question(driver);
	      String actual_id=objContentDeveloper_Question.SingleID();      	  	      
	      String titleId="title"+actual_id;
	      String ViewId="view"+actual_id;
	      driver.findElement(By.id(titleId)).sendKeys("Test Title");  	        
	      String QuestionId="question"+actual_id;
	      String KeyId="key"+actual_id;
	      Thread.sleep(500);
	      driver.findElement(By.id(QuestionId)).sendKeys("Test question data");
	      driver.findElement(By.id(KeyId)).sendKeys("Test key value data");
	      objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
	      objContentDeveloper_MetaDataLinking.MetaData_Linking();
	      Thread.sleep(500);
	      String oldTab = driver.getWindowHandle();
	      ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
	      newTab.remove(oldTab);
	      driver.switchTo().window(newTab.get(0));
	      objContentDeveloper_MetaDataLinking.Add_keyword_link();
	      Thread.sleep(500);
	      for ( String currentwindow : driver.getWindowHandles())driver.switchTo( ).window(currentwindow);
	                 {
		               Thread.sleep(400);
	                   Select dropdown=new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(objContentDeveloper_MetaDataLinking.KeyWordTypeID)));
	                   dropdown.selectByIndex(1);
	                   Thread.sleep(300);
	                   objContentDeveloper_MetaDataLinking.Assign_Keyword_Model();
	                   Thread.sleep(100);
	                   WebElement objKw=driver.findElement(objContentDeveloper.MetaDataModel);
	                   objKw.sendKeys("Nstest");
	                   objKw.sendKeys(Keys.ENTER);  
	                   Thread.sleep(300);
                    objContentDeveloper.AddKeyword();
	                   Thread.sleep(300);
	                   driver.close();		                    
	                 }   		           
	      driver.switchTo().window(oldTab);
	      Thread.sleep(300);           
	      String Note="Note"+actual_id;
          objContentDeveloper.Note();
	 	  driver.findElement(By.id(Note)).sendKeys("Test developer note data");        
	 	  String addComment="addComment"+actual_id;
	      driver.findElement(By.id(addComment)).click();
	      Thread.sleep(200); 
	      objContentDeveloper.close_developer_note();
	 	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='btnSubmit']")));
	      objContentDeveloper.Distractor_creator();
	      Thread.sleep(500);		    
	      try {
	      	   Thread.sleep(300);
	          }catch (InterruptedException e)	 {
		        String error_msg=objContentDeveloper.Error_Msg();
		        String error_msg2=objContentDeveloper.validation();
		        if (error_msg.equals(objContentDeveloper.validation_msg1))
		        {
		            System.out.println("Fail 'incomplete data'");
		        }
		        else if (error_msg2.equals(objContentDeveloper.validation_msg2))
		        {
		            System.out.println("Fail 'No Root Keyword'");
		        }
		        else
		        {
		            System.out.println(objContentDeveloper.validation_msg3);
		        }
	      
	        }
	      final String newLine1 = System.getProperty("line.separator");
	      try {
	          FileWriter writer = new FileWriter(objLogin.FilePath, true);
	          
	          writer.write(newLine1 + actual_id);
	          writer.write('\n');   
	          writer.close();
	      } catch (IOException e) {
	          e.printStackTrace();
	      }
	      Thread.sleep(500);
	      
	      
	      //Second Role ' Distractor creator' ,assume same user have both roles'Navigate to Home page'		        
	      objContentDeveloper.Home_page();
	      Thread.sleep(200);
	      objDistractorEditor.SingleDistractor();
	      Thread.sleep(300);
	      objDistractorEditor.unassigned_question();
	      WebElement subject2=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab2= new Select(subject2);
	      ab2.selectByVisibleText(objContentDeveloper.subject);
	      Thread.sleep(200);	      
	      for(int x=0;x<4;x++)
	      {
	    	  objDistractorEditor.RequestGENID();
	    	  
	      }
	      Thread.sleep(300);
	      driver.navigate().refresh();
	      objDistractorEditor.ViewGENID(ViewId); 
	      Thread.sleep(600);
	      objDistractorEditor.Submit_to_copyEditor();
	      objEnglishCopyEditor.Home_page();
	      
	      
		  //Third Role ' English Copy Editor ' ,assume same user have both roles'Navigate to Home page'		        
          objEnglishCopyEditor.EnglishCopyEditorDev();
	      WebElement subject3=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab3= new Select(subject3);
	      ab3.selectByVisibleText(objContentDeveloper.subject);		     	        
	      for(int x=0;x<4;x++)
	      {
	    	  objEnglishCopyEditor.RequestGENID();
	    	  
	      }
	      Thread.sleep(300);
	      driver.navigate().refresh();
	      Thread.sleep(1000);
	      objDistractorEditor.ViewGENID(ViewId);
	      Thread.sleep(1000);
	      driver.navigate().refresh();
	      Thread.sleep(1000);
          objEnglishCopyEditor.EnglishCopyEditorReviewer();
	      Thread.sleep(500);
	      
	      
	      //Fourth Role ' English copy editor reviewer' ,assume same user have both roles'Navigate to Home page'
	      //need to log out then login in different user
	      objEnglishCopyEditorReviewer.signout();
	      Thread.sleep(500);
	      objEnglishCopyEditorReviewer.LoginToCDS("", "");
	      Thread.sleep(500);	
	      objEnglishCopyEditorReviewer.English_copy_editor_reviewer();
	      //Select subject by visible text and request ID
	      WebElement subject4=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab4= new Select(subject4);
	      ab4.selectByVisibleText(objContentDeveloper.subject);
	      for(int x=0;x<4;x++)
	      {
	    	  objEnglishCopyEditorReviewer.RequestGENID();
	    	  
	      }
	      Thread.sleep(300);
	      driver.navigate().refresh();
	      objEnglishCopyEditorReviewer.ViewGENID(ViewId);
	      Thread.sleep(800);
	      wait.until(ExpectedConditions.visibilityOfElementLocated(objEnglishCopyEditorReviewer.Submit_To_MathReviewer));
	      objEnglishCopyEditorReviewer.Submit_To_MathReviewer();
	      Thread.sleep(500);
	      
	      
	      //Fifth Role ' Math reviewer' ,assume same user have both roles'Navigate to Home page'
	      objEnglishCopyEditorReviewer.signout();
	      Thread.sleep(500);
	      objLogin.LoginToCDS("", "");
	      Thread.sleep(200);	
	      objSingleMathReviewer.Submit_To_MathReviewer();
	      Thread.sleep(200);
	      objSingleMathReviewer.UnassignedQuestion();
	      WebElement subject5=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab5= new Select(subject5);
	      ab5.selectByVisibleText(objContentDeveloper.subject);
	      for(int x=0;x<4;x++)
	      {
	    	  objSingleMathReviewer.RequestGENID();
	    	  
	      }
	      Thread.sleep(300);
	      driver.navigate().refresh();	
	      objSingleMathReviewer.ViewGENID(ViewId);
	      Thread.sleep(600);
	      objSingleMathReviewer.Submit_To_MarkupSpecialist();	      	      
	      
	      
	      //Fifth role 'Single Markup specialist 	      
	      //Now we get ID of the single id 'old one' from txt file
	      final FileReader namereader = new FileReader(objSingleMarkupSpecialist.OldIdSingle);
	      final BufferedReader in = new BufferedReader(namereader);
	      String oldID=in.readLine();    
	      //execute rename oldid* actualID'New id' *
	      try {
	          String new_dir=objSingleMarkupSpecialist.GENID_Folder_Single;
	          Runtime rt = Runtime.getRuntime();		            
	         
				String newID=actual_id;
				String cmd = "cmd.exe /c ren" + " " +oldID+"*"+" "+newID+"*" ;
	          rt.exec(cmd.toString(), null, new File(new_dir));
	  		} catch (IOException e)
	              {
	  		        System.out.println("FROM CATCH" + e.toString());
	  		    }
	      //overwrite oldid with newid in txt file
	      Thread.sleep(500);
	       FileWriter writer = new FileWriter(objSingleMarkupSpecialist.OldIdSingle, false);      
	       writer.write(actual_id);
	       writer.close();
	       System.out.println(oldID);
	       System.out.println(actual_id);	       
	       Thread.sleep(1000);
	       String xml=objSingleMarkupSpecialist.GENID_Folder_Single+actual_id+".single.xml";
	       File inputXML = new File(xml);
	       FileReader fr = new FileReader(inputXML);
	       String s= "";   
	       StringBuilder strTotale = new StringBuilder();
	       BufferedReader br = new BufferedReader(fr);
	       try {
	           while ((s = br.readLine()) != null) {
	        	   s=s.replaceAll(oldID, actual_id);
	        	   strTotale.append(s);
	        	   }
	       } catch ( IOException  e) {
	           e.printStackTrace();
	       }  
	       finally
	       {
	               try {
	                   br.close();
	               } catch (IOException e) {
	                   e.printStackTrace();
	               }
	       }
	       PrintWriter out = new PrintWriter(xml);       
	       out.println(strTotale);
	       out.close();
	      //Xml content
	      System.out.println(strTotale.toString()); 	      
	      objSingleMarkupSpecialist.Home_page();
	      Thread.sleep(200);
	      objSingleMarkupSpecialist.SingleMarkupSpecialistDev();
	      driver.navigate().refresh();
	      Thread.sleep(500);
	      wait.until(ExpectedConditions.visibilityOfElementLocated(objSingleMarkupSpecialist.ViewID2));	   
	      WebElement subject6=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab6= new Select(subject6);
	      ab6.selectByVisibleText(objContentDeveloper.subject);
	      for(int x=0;x<4;x++)
	      {
	    	  objSingleMarkupSpecialist.RequestGENID();
	    	  
	      }
	      Thread.sleep(3000);
	      driver.navigate().refresh();
	      Thread.sleep(500);
	      driver.navigate().refresh();
	      WebElement element = driver.findElement(By.id("file"));
	      element.sendKeys(xml);	          
	      String Status=null;
	      Thread.sleep(8000); 
	      Status=objSingleMarkupSpecialist.Status();
	      System.out.println(Status.toString());
	      driver.navigate().refresh();
	      Thread.sleep(500);
	      driver.navigate().refresh();
	       while (Status.contains("Processing"))
	      {
		      Thread.sleep(6000);
		      driver.navigate().refresh();
		      Thread.sleep(2000);
		      Status=objSingleMarkupSpecialist.Status();
		      Thread.sleep(2000);
		      //Check ID status
		      //System.out.println(Status.toString());
	      }
	      objSingleMarkupSpecialist.ViewGENID(ViewId);
	      Thread.sleep(1000);
	      driver.navigate().refresh();
	      Thread.sleep(2000);
	      objSingleMarkupSpecialist.Submit_To_MarkupReviewer();
	      System.out.println(Status.toString()); 
	      Thread.sleep(2000);	      
	      
	      //Role six : Markup reviewer
	      objSingleMarkupreviewer.Home_page();
	      Thread.sleep(200);
	      objSingleMarkupreviewer.SingleMarkupReviewerDev();
	      Thread.sleep(200);
	      WebElement subject7=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab7= new Select(subject7);
	      for(int x=0;x<4;x++)
	      {
		      ab7.selectByVisibleText(objContentDeveloper.subject);
		      Thread.sleep(200);
	    	  objSingleMarkupreviewer.RequestGENID();
		      Thread.sleep(200);

	    	  
	      }
	      Thread.sleep(1000);
	      objSingleMarkupreviewer.ViewGENID(ViewId);
	      Thread.sleep(500);

	      JavascriptExecutor js = (JavascriptExecutor) driver;  
	      js.executeScript("window.scrollBy(1000,0)", "");
	      
	      Thread.sleep(1500);
	      objSingleMarkupreviewer.Comment2();
	      Thread.sleep(500);
	      objSingleMarkupreviewer.Reject_To_MarkupSpecialist();	
	      Thread.sleep(2000);
	      objSingleMarkupreviewer.Home_page();
	      Thread.sleep(200);
	      objSingleMarkupSpecialist.SingleMarkupSpecialistDev();
	      driver.navigate().refresh();
	      Thread.sleep(500);
	      wait.until(ExpectedConditions.visibilityOfElementLocated(objSingleMarkupSpecialist.ViewID2));	   
	      WebElement subject8=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab8= new Select(subject8);
	      for(int x=0;x<4;x++)
	      {
		      ab8.selectByVisibleText(objContentDeveloper.subject);
		      Thread.sleep(200);
	    	  objSingleMarkupSpecialist.RequestGENID();
		      Thread.sleep(300);
	      }

	      driver.navigate().refresh();
	      Thread.sleep(1000);
          driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
	      driver.navigate().refresh();
	      Thread.sleep(1500);
       boolean exists2 = driver.findElements(By.id(ViewId)).size() != 0;
       System.out.println(exists2);
       Thread.sleep(300);
 	  if (exists2)
 	     {
 		 System.out.println("**PASSED**:Rejection_From_Single_Markup_Reviewer_To_Markup_specialist TestCase2");

 	     }
 	  else
 	     {
 		  System.out.println("Failed!!");
 	     }
       Thread.sleep(500);
   }
    
    
    @Test(priority=3)
	  //SingleContentDeveloper
	  public void Rejection_From_Content_Writer_To_MarkupSpecialist() throws InterruptedException, IOException {	 	
	  System.out.println("Rejection_From_Content_Writer_To_MarkupSpecialist TestCase3"); 	
 	  WebDriverWait wait = new WebDriverWait(driver,25000);	    	
 	 //Content Developer
     objSingleMarkupSpecialist.Home_page();
     Thread.sleep(300);
     objContentDeveloper.clickContentDeveloper();
     objContentDeveloper.UnassignedQuestion();
     WebElement subject=driver.findElement(objContentDeveloper.SubjectType);
     Select ab= new Select(subject);
     ab.selectByVisibleText(objContentDeveloper.subject);
     objContentDeveloper.Subject_DropDownList();
     String actual_id2="view"+objContentDeveloper.GENID();      	  	      
     System.out.println(actual_id2);
     objContentDeveloper.ViewGENID(actual_id2);     Thread.sleep(1000);
     objContentDeveloper_Question = new ContentDeveloper_Question(driver);
     String actual_id=objContentDeveloper_Question.SingleID();      	  	      
     String titleId="title"+actual_id;
     String ViewId="view"+actual_id;
     driver.findElement(By.id(titleId)).sendKeys("Test Title");  	        
     String QuestionId="question"+actual_id;
     String KeyId="key"+actual_id;
     Thread.sleep(500);
     driver.findElement(By.id(QuestionId)).sendKeys("Test question data");
     driver.findElement(By.id(KeyId)).sendKeys("Test key value data");
     objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
     objContentDeveloper_MetaDataLinking.MetaData_Linking();
     Thread.sleep(500);
     String oldTab = driver.getWindowHandle();
     ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
     newTab.remove(oldTab);
     driver.switchTo().window(newTab.get(0));
     objContentDeveloper_MetaDataLinking.Add_keyword_link();
     Thread.sleep(500);
     for ( String currentwindow : driver.getWindowHandles())driver.switchTo( ).window(currentwindow);
                {
	               Thread.sleep(400);
                  Select dropdown=new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(objContentDeveloper_MetaDataLinking.KeyWordTypeID)));
                  dropdown.selectByIndex(1);
                  Thread.sleep(300);
                  objContentDeveloper_MetaDataLinking.Assign_Keyword_Model();
                  Thread.sleep(100);
                  WebElement objKw=driver.findElement(objContentDeveloper.MetaDataModel);
                  objKw.sendKeys("Nstest");
                  objKw.sendKeys(Keys.ENTER);  
                  Thread.sleep(300);
                  objContentDeveloper.AddKeyword();
                  Thread.sleep(300);
                  driver.close();		                    
                }   		           
     driver.switchTo().window(oldTab);
     Thread.sleep(300);           
     String Note="Note"+actual_id;
     objContentDeveloper.Note();
	  driver.findElement(By.id(Note)).sendKeys("Test developer note data");        
	  String addComment="addComment"+actual_id;
     driver.findElement(By.id(addComment)).click();
     Thread.sleep(200); 
     objContentDeveloper.close_developer_note();
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='btnSubmit']")));
     objContentDeveloper.Distractor_creator();
     Thread.sleep(500);		    
     try {
     	   Thread.sleep(300);
         }catch (InterruptedException e)	 {
	        String error_msg=objContentDeveloper.Error_Msg();
	        String error_msg2=objContentDeveloper.validation();
	        if (error_msg.equals(objContentDeveloper.validation_msg1))
	        {
	            System.out.println("Fail 'incomplete data'");
	        }
	        else if (error_msg2.equals(objContentDeveloper.validation_msg2))
	        {
	            System.out.println("Fail 'No Root Keyword'");
	        }
	        else
	        {
	            System.out.println(objContentDeveloper.validation_msg3);
	        }
     
       }
     final String newLine1 = System.getProperty("line.separator");
     try {
         FileWriter writer = new FileWriter(objLogin.FilePath, true);
         
         writer.write(newLine1 + actual_id);
         writer.write('\n');   
         writer.close();
     } catch (IOException e) {
         e.printStackTrace();
     }
     Thread.sleep(500);
     
     
     //Second Role ' Distractor creator' ,assume same user have both roles'Navigate to Home page'		        
     objContentDeveloper.Home_page();
     Thread.sleep(200);
     objDistractorEditor.SingleDistractor();
     Thread.sleep(300);
     objDistractorEditor.unassigned_question();
     WebElement subject2=driver.findElement(objContentDeveloper.SubjectType);
     Select ab2= new Select(subject2);
     ab2.selectByVisibleText(objContentDeveloper.subject);
     Thread.sleep(200);	      
     for(int x=0;x<4;x++)
     {
    	 objDistractorEditor.RequestGENID();
   	  
     }
     Thread.sleep(300);
     driver.navigate().refresh();
     objDistractorEditor.ViewGENID(ViewId); 
     Thread.sleep(600);
     objDistractorEditor.Submit_to_copyEditor();
     objEnglishCopyEditor.Home_page();
     
     
	  //Third Role ' English Copy Editor ' ,assume same user have both roles'Navigate to Home page'		        
     objEnglishCopyEditor.EnglishCopyEditorDev();
     WebElement subject3=driver.findElement(objContentDeveloper.SubjectType);
     Select ab3= new Select(subject3);
     ab3.selectByVisibleText(objContentDeveloper.subject);		     	        
     for(int x=0;x<4;x++)
     {
    	 objEnglishCopyEditor.RequestGENID();
   	  
     }
     Thread.sleep(300);
     driver.navigate().refresh();
     objEnglishCopyEditor.ViewGENID(ViewId);
     Thread.sleep(900);
     objEnglishCopyEditor.EnglishCopyEditorReviewer();
     Thread.sleep(500);
     
     
     //Fourth Role ' English copy editor reviewer' ,assume same user have both roles'Navigate to Home page'
     //need to log out then login in different user
     objEnglishCopyEditorReviewer.signout();
     Thread.sleep(500);
     objEnglishCopyEditorReviewer.LoginToCDS("", "");
     Thread.sleep(500);	
     objEnglishCopyEditorReviewer.English_copy_editor_reviewer();
     //Select subject by visible text and request ID
     WebElement subject4=driver.findElement(objContentDeveloper.SubjectType);
     Select ab4= new Select(subject4);
     ab4.selectByVisibleText(objContentDeveloper.subject);
     for(int x=0;x<4;x++)
     {
    	 objEnglishCopyEditorReviewer.RequestGENID();
   	  
     }
     Thread.sleep(300);
     driver.navigate().refresh();
     objEnglishCopyEditorReviewer.ViewGENID(ViewId);
     Thread.sleep(800);
     wait.until(ExpectedConditions.visibilityOfElementLocated(objEnglishCopyEditorReviewer.Submit_To_MathReviewer));
     objEnglishCopyEditorReviewer.Submit_To_MathReviewer();
     Thread.sleep(500);              
      //Fifth Role ' Math reviewer' ,assume same user have both roles'Navigate to Home page'
	      objEnglishCopyEditorReviewer.signout();
	      Thread.sleep(500);
	      objLogin.LoginToCDS("", "");
	      Thread.sleep(200);	
	      objSingleMathReviewer.Submit_To_MathReviewer();
	      Thread.sleep(200);
	      objSingleMathReviewer.UnassignedQuestion();
	      WebElement subject5=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab5= new Select(subject5);
	      ab5.selectByVisibleText(objContentDeveloper.subject);
	      for(int x=0;x<4;x++)
	      {
	    	  objSingleMathReviewer.RequestGENID();
	    	  
	      }
	      Thread.sleep(300);
	      driver.navigate().refresh();	
	      objSingleMathReviewer.ViewGENID(ViewId);
	      Thread.sleep(600);
	      objSingleMathReviewer.Submit_To_MarkupSpecialist();	      	      	     	      
	      //Fifth role 'Single Markup specialist 	      
	      //Now we get ID of the single id 'old one' from txt file
	      final FileReader namereader = new FileReader(objSingleMarkupSpecialist.OldIdSingle);
	      final BufferedReader in = new BufferedReader(namereader);
	      String oldID=in.readLine();    
	      //execute rename oldid* actualID'New id' *
	      try {
	          String new_dir=objSingleMarkupSpecialist.GENID_Folder_Single;
	          Runtime rt = Runtime.getRuntime();		            
	         
				String newID=actual_id;
				String cmd = "cmd.exe /c ren" + " " +oldID+"*"+" "+newID+"*" ;
	          rt.exec(cmd.toString(), null, new File(new_dir));
	  		} catch (IOException e)
	              {
	  		        System.out.println("FROM CATCH" + e.toString());
	  		    }
	      //overwrite oldid with newid in txt file
	      Thread.sleep(500);
	       FileWriter writer = new FileWriter(objSingleMarkupSpecialist.OldIdSingle, false);      
	       writer.write(actual_id);
	       writer.close();
	       System.out.println(oldID);
	       System.out.println(actual_id);	       
	       Thread.sleep(1000);
	       String xml=objSingleMarkupSpecialist.GENID_Folder_Single+actual_id+".single.xml";
	       File inputXML = new File(xml);
	       FileReader fr = new FileReader(inputXML);
	       String s= "";   
	       StringBuilder strTotale = new StringBuilder();
	       BufferedReader br = new BufferedReader(fr);
	       try {
	           while ((s = br.readLine()) != null) {
	        	   s=s.replaceAll(oldID, actual_id);
	        	   strTotale.append(s);
	        	   }
	       } catch ( IOException  e) {
	           e.printStackTrace();
	       }  
	       finally
	       {
	               try {
	                   br.close();
	               } catch (IOException e) {
	                   e.printStackTrace();
	               }
	       }
	       PrintWriter out = new PrintWriter(xml);       
	       out.println(strTotale);
	       out.close();
	      //Xml content
	      System.out.println(strTotale.toString()); 	      
	      objSingleMarkupSpecialist.Home_page();
	      Thread.sleep(200);
	      objSingleMarkupSpecialist.SingleMarkupSpecialistDev();
	      driver.navigate().refresh();
	      Thread.sleep(500);
	      wait.until(ExpectedConditions.visibilityOfElementLocated(objSingleMarkupSpecialist.ViewID2));	   
	      WebElement subject6=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab6= new Select(subject6);
	      ab6.selectByVisibleText(objContentDeveloper.subject);
	      for(int x=0;x<4;x++)
	      {
	    	  objSingleMarkupSpecialist.RequestGENID();
	    	  
	      }
	      Thread.sleep(3000);
	      driver.navigate().refresh();
	      Thread.sleep(500);
	      driver.navigate().refresh();
	      WebElement element = driver.findElement(By.id("file"));
	      element.sendKeys(xml);	          
	      String Status=null;
	      Thread.sleep(8000); 
	      Status=objSingleMarkupSpecialist.Status();
	      System.out.println(Status.toString());
	      driver.navigate().refresh();
	      Thread.sleep(500);
	      driver.navigate().refresh();
	       while (Status.contains("Processing"))
	      {
		      Thread.sleep(6000);
		      driver.navigate().refresh();
		      Thread.sleep(2000);
		      Status=objSingleMarkupSpecialist.Status();
		      Thread.sleep(2000);
		      //Check ID status
		      //System.out.println(Status.toString());
	      }		  
		  System.out.println(Status.toString()); 
	      driver.navigate().refresh();
	      Thread.sleep(500);
	      objSingleMarkupSpecialist.ViewGENID(ViewId);
	      Thread.sleep(2000);
	      driver.navigate().refresh();
	      Thread.sleep(2500);
	      objSingleMarkupSpecialist.Submit_To_MarkupReviewer();
	      Thread.sleep(2000);	      	      
	      //Role six : Markup reviewer
	      objSingleMarkupreviewer.Home_page();
	      Thread.sleep(200);
	      objSingleMarkupreviewer.SingleMarkupReviewerDev();
	      Thread.sleep(200);
	      WebElement subject7=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab7= new Select(subject7);
	      ab7.selectByVisibleText(objContentDeveloper.subject);
	      for(int x=0;x<4;x++)
	      {
	    	  objSingleMarkupreviewer.RequestGENID();
	    	  
	      }
	      Thread.sleep(1000);
	      driver.navigate().refresh();
	      Thread.sleep(500);
	      objSingleMarkupreviewer.ViewGENID(ViewId);
	      Thread.sleep(5000);
	      objSingleMarkupreviewer.Submit_To_ContentWriter();
	      Thread.sleep(2000);
	      objSingleContentWriter.Home_page();
	      Thread.sleep(200);      
	      
	      //Role seven :Content Writer
	      objSingleContentWriter.SingleContentWriterDev();
	      objSingleContentWriter.UnassignedQuestion();
	      WebElement subject8=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab8= new Select(subject8);
	      ab8.selectByVisibleText(objContentDeveloper.subject);
	      for(int x=0;x<4;x++)
	      {
	    	  objSingleContentWriter.RequestGENID();
	    	  
	      }
	      Thread.sleep(300);
	      driver.navigate().refresh();
	      objSingleContentWriter.ViewGENID(ViewId);
	      Thread.sleep(500);
	      driver.navigate().refresh();
	      Thread.sleep(1000);
	      wait.until(ExpectedConditions.visibilityOfElementLocated(objSingleContentWriter.Approve));    
	      objSingleMarkupreviewer.Comment();
	      Thread.sleep(500);
	      objSingleMathReviewer.Reject_To_ContentDevloper();	
	      Thread.sleep(2000);
	      objSingleMarkupreviewer.Home_page();
	      Thread.sleep(200);
	      objSingleMarkupSpecialist.SingleMarkupSpecialistDev();
	      driver.navigate().refresh();
	      Thread.sleep(500);
	      wait.until(ExpectedConditions.visibilityOfElementLocated(objSingleMarkupSpecialist.ViewID2));	   
	      WebElement subject9=driver.findElement(objContentDeveloper.SubjectType);
	      Select ab9= new Select(subject9);
	      ab9.selectByVisibleText(objContentDeveloper.subject);
	      for(int x=0;x<4;x++)
	      {
	    	  objSingleMarkupSpecialist.RequestGENID();
	    	  
	      }
	      driver.navigate().refresh();
	      Thread.sleep(2000);
          driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
	      driver.navigate().refresh();

          boolean exists2 = driver.findElements(By.id(ViewId)).size() != 0;
          Thread.sleep(300);
          System.out.println(exists2);
	      if (exists2)
	      {
	     	 System.out.println("**PASSED**:Rejection_From_Single_ContentWriter_To_Markup_specialist TestCase3");

	      }
	     else
	     {
		     System.out.println("Failed!!");
	     }
     Thread.sleep(500);
 }
    
    @AfterMethod 
	 public void takeScreenShotOnFailure(ITestResult testResult) throws IOException { 
	 	if (testResult.getStatus() == ITestResult.FAILURE) { 
	 		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); 
	 		FileUtils.copyFile(scrFile, new File("errorScreenshots\\" + testResult.getName() + "-" 
	 				+ Arrays.toString(testResult.getParameters()) +  ".jpg"));
	 	} 
	 }
    
    
    @AfterTest
    
    public void End(){

	    driver.close();
    }

}
