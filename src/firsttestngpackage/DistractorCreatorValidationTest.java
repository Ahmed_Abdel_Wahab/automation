package firsttestngpackage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pom_pages.ContentDeveloper;
import pom_pages.ContentDeveloper_MetaDataLinking;
import pom_pages.ContentDeveloper_Question;
import pom_pages.DistractorEditor;
import pom_pages.EnglishCopyEditor;
import pom_pages.LoginPage;

public class DistractorCreatorValidationTest {

	    WebDriver driver;
	    LoginPage objLogin;
	    ContentDeveloper objContentDeveloper;
	    ContentDeveloper_Question objContentDeveloper_Question;
	    ContentDeveloper_MetaDataLinking objContentDeveloper_MetaDataLinking;
	    DistractorEditor objDistractorEditor;
	    EnglishCopyEditor objEnglishCopyEditor;


	    
	    @BeforeTest

	    public void setup() throws InterruptedException{

		    System.setProperty("webdriver.chrome.driver","Resources/chromedriver.exe");
		    ChromeOptions options = new ChromeOptions();
		    options.setHeadless(LoginPage.ChromeDriverOption);
	        driver = new ChromeDriver(options);
	        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		    driver.get("http://testing.cds.nagwa.com");
		    driver.navigate().refresh();
		    Thread.sleep(400);
			objLogin = new LoginPage(driver);
		    objLogin.LoginToCDS("", "");
			objContentDeveloper = new ContentDeveloper(driver);
			objContentDeveloper_Question = new ContentDeveloper_Question(driver);
			objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
			objDistractorEditor = new DistractorEditor(driver);
			objEnglishCopyEditor = new EnglishCopyEditor(driver);

	    }
	    
	    @Test(priority=1)
		  //SingleContentDeveloper
		  public void Check_KeyValue_Validation_If_Missing() throws InterruptedException, IOException {	
	    	
			  System.out.println("Single Distractor Creator Validation Method1 TestCase1 'Check required Distractors data if missing");
	    	
	    	  WebDriverWait wait = new WebDriverWait(driver,25000);	    	
	    	//Content Developer
		      objContentDeveloper.clickContentDeveloper();
		      objContentDeveloper.UnassignedQuestion();
		      WebElement subject=driver.findElement(objContentDeveloper.SubjectType);
		      Select ab= new Select(subject);
		      ab.selectByVisibleText(objContentDeveloper.subject);
		      objContentDeveloper.Subject_DropDownList();
		      String actual_id2="view"+objContentDeveloper.GENID();      	  	      
		      System.out.println(actual_id2);
		      objContentDeveloper.ViewGENID(actual_id2);
		      Thread.sleep(1000);
		      objContentDeveloper_Question = new ContentDeveloper_Question(driver);
		      String actual_id=objContentDeveloper_Question.SingleID();      	  	      
		      String titleId="title"+actual_id;
		      String ViewId="view"+actual_id;
		      driver.findElement(By.id(titleId)).sendKeys("Test Title");  	        
		      String QuestionId="question"+actual_id;
		      String KeyId="key"+actual_id;
		      Thread.sleep(500);
		      driver.findElement(By.id(QuestionId)).sendKeys("Test question data");
		      driver.findElement(By.id(KeyId)).sendKeys("Test key value data");
		      objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
		      objContentDeveloper_MetaDataLinking.MetaData_Linking();
		      Thread.sleep(500);
		      String oldTab = driver.getWindowHandle();
		      ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		      newTab.remove(oldTab);
		      driver.switchTo().window(newTab.get(0));
		      objContentDeveloper_MetaDataLinking.Add_keyword_link();
		      Thread.sleep(500);
		      for ( String currentwindow : driver.getWindowHandles())driver.switchTo( ).window(currentwindow);
		                 {
			               Thread.sleep(400);
		                   Select dropdown=new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(objContentDeveloper_MetaDataLinking.KeyWordTypeID)));
		                   dropdown.selectByIndex(1);
		                   Thread.sleep(300);
		                   objContentDeveloper_MetaDataLinking.Assign_Keyword_Model();
		                   Thread.sleep(100);
		                   WebElement objKw=driver.findElement(objContentDeveloper.MetaDataModel);
		                   objKw.sendKeys("Nstest");
		                   objKw.sendKeys(Keys.ENTER);  
		                   Thread.sleep(300);
	                       objContentDeveloper.AddKeyword();
		                   Thread.sleep(300);
		                   driver.close();		                    
		                 }   		           
		      driver.switchTo().window(oldTab);
		      Thread.sleep(300);           
		      String Note="Note"+actual_id;
	          objContentDeveloper.Note();
		 	  driver.findElement(By.id(Note)).sendKeys("Test developer note data");        
		 	  String addComment="addComment"+actual_id;
		      driver.findElement(By.id(addComment)).click();
		      Thread.sleep(200); 
		      objContentDeveloper.close_developer_note();
		 	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='btnSubmit']")));
		      objContentDeveloper.Distractor_creator();
		      Thread.sleep(500);		    
		      
		      //Second Role ' Distractor creator' ,assume same user have both roles'Navigate to Home page'		        
		      objContentDeveloper.Home_page();
		      Thread.sleep(200);
		      objDistractorEditor.SingleDistractor();
		      Thread.sleep(300);
		      objDistractorEditor.unassigned_question();
		      WebElement subject2=driver.findElement(objContentDeveloper.SubjectType);
		      Select ab2= new Select(subject2);
		      ab2.selectByVisibleText(objContentDeveloper.subject);
		      Thread.sleep(200);	      
		      for(int x=0;x<4;x++)
		      {
		    	  objDistractorEditor.RequestGENID();
		    	  
		      }

		      Thread.sleep(300);
		      driver.navigate().refresh();
		      objDistractorEditor.ViewGENID(ViewId); 
		      Thread.sleep(600);
		      driver.findElement(By.id(KeyId)).clear();
		      Thread.sleep(500);
		      objDistractorEditor.Submit_to_copyEditor();
		      String error_msg=objContentDeveloper.Error_Msg();
		      Thread.sleep(2000);
			  if (error_msg.equals(objContentDeveloper.validation_msg1))
			     {
			     System.out.println("***Passed1!***:Single Distractor Creator Validation Method1 TestCase1 'Check required KeyValue data if cleared");
			     }
			  else
			     {
			     System.out.println("Failed!");
			     }

		      

	    }
	    
	    @Test(priority=2)
		  //SingleContentDeveloper
		  public void Check_Distractors_Validation_If_Missing() throws InterruptedException, IOException {	
	    	
			  System.out.println("Single Distractor Creator Validation Method2 TestCase2 'Check required Distractors data if missing");
	    				  
	    	  WebDriverWait wait = new WebDriverWait(driver,25000);	    	
	    	  
	    	  //Back to home page
		      objContentDeveloper.Home_page();
		      Thread.sleep(300);
	       	  //Content Developer
		      objContentDeveloper.clickContentDeveloper();
		      objContentDeveloper.UnassignedQuestion();
		      WebElement subject=driver.findElement(objContentDeveloper.SubjectType);
		      Select ab= new Select(subject);
		      ab.selectByVisibleText(objContentDeveloper.subject);
		      objContentDeveloper.Subject_DropDownList();
		      String actual_id2="view"+objContentDeveloper.GENID();      	  	      
		      System.out.println(actual_id2);
		      objContentDeveloper.ViewGENID(actual_id2);
		      Thread.sleep(1000);
		      objContentDeveloper_Question = new ContentDeveloper_Question(driver);
		      String actual_id=objContentDeveloper_Question.SingleID();      	  	      
		      String titleId="title"+actual_id;
		      driver.findElement(By.id(titleId)).sendKeys("Test Title");  	        
		      String QuestionId="question"+actual_id;
		      String KeyId="key"+actual_id;
		      String ViewId="view"+actual_id;
		      Thread.sleep(500);
		      driver.findElement(By.id(QuestionId)).sendKeys("Test question data");
		      driver.findElement(By.id(KeyId)).sendKeys("Test key value data");
		      objContentDeveloper_MetaDataLinking = new ContentDeveloper_MetaDataLinking(driver);
		      objContentDeveloper_MetaDataLinking.MetaData_Linking();
		      Thread.sleep(500);
		      String oldTab = driver.getWindowHandle();
		      ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		      newTab.remove(oldTab);
		      driver.switchTo().window(newTab.get(0));
		      objContentDeveloper_MetaDataLinking.Add_keyword_link();
		      Thread.sleep(500);
		      for ( String currentwindow : driver.getWindowHandles())driver.switchTo( ).window(currentwindow);
		                 {
			               Thread.sleep(400);
		                   Select dropdown=new Select(wait.until(ExpectedConditions.visibilityOfElementLocated(objContentDeveloper_MetaDataLinking.KeyWordTypeID)));
		                   dropdown.selectByIndex(1);
		                   Thread.sleep(300);
		                   objContentDeveloper_MetaDataLinking.Assign_Keyword_Model();
		                   Thread.sleep(100);
		                   WebElement objKw=driver.findElement(objContentDeveloper.MetaDataModel);
		                   objKw.sendKeys("Nstest");
		                   objKw.sendKeys(Keys.ENTER);  
		                   Thread.sleep(300);
	                       objContentDeveloper.AddKeyword();
		                   Thread.sleep(300);
		                   driver.close();		                    
		                 }   		           
		      driver.switchTo().window(oldTab);
		      Thread.sleep(300);           
		      String Note="Note"+actual_id;
	          objContentDeveloper.Note();
		 	  driver.findElement(By.id(Note)).sendKeys("Test developer note data");        
		 	  String addComment="addComment"+actual_id;
		      driver.findElement(By.id(addComment)).click();
		      Thread.sleep(200); 
		      objContentDeveloper.close_developer_note();
		 	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='btnSubmit']")));
		 	//objDistractorEditor.AddDistractor();
		      objContentDeveloper.Distractor_creator();
		      Thread.sleep(500);		    
		      
		      //Second Role ' Distractor creator' ,assume same user have both roles'Navigate to Home page'		        
		      objContentDeveloper.Home_page();
		      Thread.sleep(200);
		      objDistractorEditor.SingleDistractor();
		      Thread.sleep(300);
		      objDistractorEditor.unassigned_question();
		      WebElement subject2=driver.findElement(objContentDeveloper.SubjectType);
		      Select ab2= new Select(subject2);
		      ab2.selectByVisibleText(objContentDeveloper.subject);
		      Thread.sleep(200);	      
		      for(int x=0;x<4;x++)
		      {
		    	  objDistractorEditor.RequestGENID();
		    	  
		      }

		      Thread.sleep(300);
		      driver.navigate().refresh();
		      objDistractorEditor.ViewGENID(ViewId); 
		      Thread.sleep(600);
		      objDistractorEditor.AddDistractor();
		      Thread.sleep(600);
		      objDistractorEditor.Submit_to_copyEditor();
		      String error_msg=objContentDeveloper.Error_Msg();
		      Thread.sleep(2000);
			  if (error_msg.equals(objContentDeveloper.validation_msg1))
			     {
			     System.out.println("***Passed2!***:Single Distractor Creator Validation Method2 TestCase2 'Check required Distractors data if missing");
			     }
			  else
			     {
			     System.out.println("Failed!");
			     }

		      

	    }
	    
	    
	    @AfterMethod 
		 public void takeScreenShotOnFailure(ITestResult testResult) throws IOException { 
		 	if (testResult.getStatus() == ITestResult.FAILURE) { 
		 		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); 
		 		FileUtils.copyFile(scrFile, new File("errorScreenshots\\" + testResult.getName() + "-" 
		 				+ Arrays.toString(testResult.getParameters()) +  ".jpg"));
		 	} 
		 }
	    @AfterTest
	    
	    public void End(){

		    driver.close();
	    }

	   
}
